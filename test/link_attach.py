#!/usr/bin/env python3

import rospy
from gazebo_ros_link_attacher.srv import Attach, AttachRequest, AttachResponse


if __name__ == '__main__':
    rospy.init_node('demo_attach_links')
    # attach_srv = rospy.ServiceProxy('/link_attacher_node/attach', Attach)
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach', Attach)
    attach_srv.wait_for_service()

    # Link them
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "link5" # gripper_link, end_effector_link, link5
    req.model_name_2 = "block0"
    req.link_name_2 = "base_footprint" # base_footprint, base_link

    # req.model_name_1 = "robot" # turtlebot3_burger
    # req.link_name_1 = "base_footprint"
    # req.model_name_2 = "block0"
    # req.link_name_2 = "base_footprint"

    attach_srv.call(req)