import rospy
import tf
import numpy as np
from teaming_simulation.msg import BrodcastPose


class PriorityMonitor:
    def __init__(self, int2id, id2int, priority):
        self.int2id = int2id
        self.id2int = id2int
        self.priority = priority
        self.robot_num = len(int2id)

        self.robot_bases = []
        self.listener = tf.TransformListener()
        for i_robot in range(self.robot_num):
            robot_id = self.int2id[i_robot]
            self.robot_bases.append("robot" + str(robot_id) + "/base_footprint")

        pose_rate = rospy.Duration(0.05)
        rospy.Timer(pose_rate, self.updatePose)

        self.robot_poses = np.zeros((self.robot_num, 2), dtype=np.float64)

        self.pose_pub = rospy.Publisher("robot_poses", BrodcastPose, queue_size=10)
        print("pose_rate:", pose_rate)

    def updatePose(self, msg=None):
        for i_robot in range(self.robot_num):
            robot_id = self.int2id[i_robot]
            try:
                (trans,rot) =self.listener.lookupTransform('/map', self.robot_bases[i_robot], rospy.Time(0))
                self.robot_poses[i_robot, 0] = trans[0]
                self.robot_poses[i_robot, 1] = trans[1]
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass
        pose_msg = BrodcastPose()
        pose_msg.robot_id = self.int2id
        pose_msg.robot_priority = self.priority
        pose_msg.robot_x = self.robot_poses[:, 0].tolist()
        pose_msg.robot_y = self.robot_poses[:, 1].tolist()
        self.pose_pub.publish(pose_msg)
        # print('pose_msg = ', pose_msg)
