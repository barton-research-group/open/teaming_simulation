import rospy
import tf
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from gazebo_msgs.srv import GetModelState, GetModelStateRequest
from tf.transformations import euler_from_quaternion

class GazeboPosePublisher:
    def __init__(self, robot_link, robot_message, publish_time, flag_verbose = False, flag_publish_odom = False, robot_odom_message = ''):

        self.flag_message_type = 0

        # rospy.wait_for_service('/gazebo/get_model_state')
        self.get_model_srv = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        self.model = GetModelStateRequest()
        self.model.model_name = robot_link
        self.robot_message = robot_message
        self.flag_verbose = flag_verbose
        self.prev_time = -1.0
        self.flag_publish_odom = flag_publish_odom
        self.robot_odom_message = robot_odom_message

        self.publish_time = publish_time

        publish_rate = rospy.Duration(publish_time)
        rospy.Timer(publish_rate, self.publishCallback)

        if self.flag_message_type == 0:
            self.tf_broadcaster = tf.TransformBroadcaster()
        else:
            self.odom_pub = rospy.Publisher('/my_odom', Odometry, queue_size=10)
            self.odom_msg = Odometry()
            self.header = Header()
            self.header.frame_id= robot_link

    def publishCallback(self, msg=None):
        try:
            result = self.get_model_srv(self.model)
            if self.flag_verbose:
                gazebo_trans = (result.pose.position.x, result.pose.position.y, result.pose.position.z)
                gazebo_rot = (result.pose.orientation.x, result.pose.orientation.y, result.pose.orientation.z, result.pose.orientation.w)
                gazebo_euler = euler_from_quaternion(gazebo_rot)
                print('Trans: %.2f,  %.2f,  %.2f; Angle: %.2f,  %.2f,  %.2f' % (gazebo_trans[0], gazebo_trans[1], gazebo_trans[2], gazebo_euler[0], gazebo_euler[1], gazebo_euler[2]))
            temp_stamp = rospy.Time.now()
            if temp_stamp.to_sec() <= (self.prev_time + self.publish_time / 2.0):
                return
            if self.flag_message_type == 0:
                self.tf_broadcaster.sendTransform((result.pose.position.x, result.pose.position.y, result.pose.position.z),
                                (result.pose.orientation.x, result.pose.orientation.y, result.pose.orientation.z, result.pose.orientation.w),
                                temp_stamp,
                                self.robot_message,
                                "map")
                if self.flag_publish_odom:
                    self.tf_broadcaster.sendTransform((0.0, 0.0, 0.0), (0.0, 0.0, 0.0, 1.0), temp_stamp, self.robot_odom_message, "map")
            else:
                self.odom_msg.pose.pose = result.pose
                self.odom_msg.twist.twist = result.twist
                self.header.stamp = temp_stamp
                self.odom_msg.header = self.header
                self.odom_pub.publish (self.odom_msg)
            self.prev_time = temp_stamp.to_sec()
            print('self.prev_time = ', self.prev_time)
            # print(result)
        except:
            print('GazeboPosePublisher: Not able to publish Gazebo poses!')
            return