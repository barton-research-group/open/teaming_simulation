from teaming_simulation.Constant import Constant
import yaml

class CaseIO():
    def __init__(self):
        self.str_before = ""
        self.str_after = ""
        self.str = ""
        self.task_stage = 0

        self.flag_gtpose = False

        self.initialize()

        self.world_name = 'square'

        self.robot_type_list = [Constant.ROB_TYPE_LARGE_HEAVY, Constant.ROB_TYPE_LARGE]
        self.robot_x_list = [-2.0, -1.0]
        self.robot_y_list = [-1.0, -0.5]

        self.block_type_list = [Constant.BLOCK_LIGHT, Constant.BLOCK_HEAVY, Constant.BLOCK_LIGHT]
        self.block_x_list = [ 0.904,  2.000, 2.000]
        self.block_y_list = [-3.484, -1.084, 1.000]

        self.result_yaml = None
    
    def updateString(self):
        self.str = ""
        self.addWorld(self.world_name)
        self.addRobot(self.robot_type_list, self.robot_x_list, self.robot_y_list)
        self.addBlock(self.block_type_list, self.block_x_list, self.block_y_list)

    def readYAML(self, file_name, flag_return = False):
        with open(file_name) as yaml_file:
            yaml_dict = yaml.full_load(yaml_file)
            # print('yaml_dict = ', yaml_dict)
        self.robot_type_list = yaml_dict['robot_type']
        self.robot_x_list = yaml_dict['robot_x']
        self.robot_y_list = yaml_dict['robot_y']
        self.block_type_list = yaml_dict['block_type']
        self.block_x_list = yaml_dict['block_x']
        self.block_y_list = yaml_dict['block_y']
        self.world_name = yaml_dict['world_name']
        if 'result_yaml' in yaml_dict:
            self.result_yaml = yaml_dict['result_yaml']
        if flag_return:
            return yaml_dict

    def writeYAML(self, file_name):
        yaml_dict = {}
        yaml_dict['robot_type'] = self.robot_type_list
        yaml_dict['robot_x'] = self.robot_x_list
        yaml_dict['robot_y'] = self.robot_y_list
        yaml_dict['block_type'] = self.block_type_list
        yaml_dict['block_x'] = self.block_x_list
        yaml_dict['block_y'] = self.block_y_list
        yaml_dict['world_name'] = self.world_name
        if self.result_yaml:
            yaml_dict['result_yaml'] = self.result_yaml
        with open(file_name, 'w') as yaml_file:
            yaml.dump(yaml_dict, yaml_file, default_flow_style=None)

    def writeLaunch(self, file_name):
        self.updateString()
        write_str = self.str_before + self.str + self.str_after
        launch_file = open(file_name, "w")
        launch_file.write(write_str)
        launch_file.close()
    
    def getRobotParam(self, robot_type):
        if robot_type == Constant.ROB_TYPE_SMALL:
            file_name = 'one_robot.launch'
            model = 'burger'
            urdf_model = 'burger'
        elif robot_type == Constant.ROB_TYPE_SMALL_BROAD:
            file_name = 'one_robot.launch'
            model = 'burger'
            urdf_model = 'burger1'
        elif robot_type == Constant.ROB_TYPE_LARGE_NOARM:
            file_name = 'one_robot.launch'
            model = 'waffle'
            urdf_model = 'waffle'
        elif robot_type == Constant.ROB_TYPE_LARGE:
            file_name = 'one_robot_witharm.launch'
            model = 'waffle'
            urdf_model = 'waffle'
        else: # ROB_TYPE_LARGE_HEAVY
            file_name = 'one_robot_witharm.launch'
            model = 'waffle'
            urdf_model = 'waffle1'
        return file_name, model, urdf_model
    
    def addRobot(self, robot_type_list, robot_x_list, robot_y_list):
        robot_num = len(robot_type_list)
        for i_robot in range(robot_num):
            robot_id = "robot" + str(i_robot+1)
            file_name, model, urdf_model = self.getRobotParam(robot_type_list[i_robot])
            self.str +='''
  <group ns="''' + robot_id + '''">
    <param name="tf_prefix" value="''' + robot_id + '''" />
    <include file="$(find teaming_simulation)/launch/''' + file_name +'''" >
      <arg name="x_pose" value= "''' + str(robot_x_list[i_robot]) + '''"/>
      <arg name="y_pose" value= "''' + str(robot_y_list[i_robot]) + '''"/>
      <arg name="z_pose" value= "''' + "0.0" + '''"/>
      <arg name="robot_name"  value="''' + robot_id + '''" />
      <arg name="model"  value= "''' + model + '''" />
      <arg name="urdf_model"  value= "''' + urdf_model + '''" />
    </include>
  </group>
'''

        if self.flag_gtpose and robot_num >= 1:
            self.str +='''
  <node pkg="teaming_simulation" name="robot_pose_publisher" type="gazebo_pose_publisher.py" args="''' + str(robot_num) + ''' robot 1 0.01"> </node>>
'''

    def addBlock(self, block_type_list, block_x_list, block_y_list):
        block_num = len(block_type_list)
        for i_block in range(block_num):
            block_id = "block" + str(i_block)
            if block_type_list[i_block] == Constant.BLOCK_LIGHT:
                model = 'block'
            else:
                model = 'block_heavy'
            self.str +='''
  <group ns="''' + block_id + '''">
    <param name="tf_prefix" value="''' + block_id + '''" />
    <include file="$(find teaming_simulation)/launch/''' + "block.launch" +'''" >
      <arg name="x_pose" value= "''' + str(block_x_list[i_block]) + '''"/>
      <arg name="y_pose" value= "''' + str(block_y_list[i_block]) + '''"/>
      <arg name="z_pose" value= "''' + "0.0" + '''"/>
      <arg name="robot_name"  value="''' + block_id + '''" />
      <arg name="model"  value= "''' + model + '''" />
    </include>
  </group>
'''
        if block_num >= 1:
            self.str +='''
  <node pkg="teaming_simulation" name="gazebo_pose_publisher" type="gazebo_pose_publisher.py" args="''' + str(block_num) + ''' block 0 0.1"> </node>>
'''

    def addWorld(self, world_name):
        self.str += '''
  <include file="$(find gazebo_ros)/launch/empty_world.launch">
    <arg name="world_name" value="$(find teaming_simulation)/worlds/''' + world_name + '''.world"/>
    <arg name="paused" value="false"/>
    <arg name="use_sim_time" value="true"/>
    <arg name="gui" value="true"/>
    <arg name="headless" value="false"/>
    <arg name="debug" value="false"/>
  </include>
  
   <!-- Map server -->
    <node pkg="map_server" name="map_server" type="map_server" args="$(find teaming_simulation)/worlds/''' + world_name + '''.yaml">
      <param name="frame_id" value="map" />
        <!-- <remap from="/map" to="robot1/map"/> -->
    </node>>
'''

    def initialize(self):
        self.str_before = '''<launch>
  <arg name="model1" default="burger"/>
  <arg name="model2" default="waffle"/>
  <arg name="open_rviz" default="true"/>

  <plugin name="ros_link_attacher_plugin" filename="libgazebo_ros_link_attacher.so"/>
'''
        self.str_after = '''
  <group if="$(arg open_rviz)">
    <node pkg="rviz" type="rviz" name="rviz" required="true"
          args="-d $(find teaming_simulation)/rviz/teaming_simulation.rviz"/>
  </group>
</launch>
'''
