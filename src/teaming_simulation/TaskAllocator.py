import numpy as np
import rospy
from visualization_msgs.msg import Marker, MarkerArray
from teaming_simulation.GridMap import GridMap
import teaming_simulation.Visualization as Visualization
from teaming_simulation.msg import RobotVisualization, RobotAction, RobotActionFeedback
from teaming_simulation.Constant import Constant
from gazebo_msgs.srv import GetModelState, GetModelStateRequest
from scipy.spatial.distance import cdist
import yaml
import time

flag_ping = True
if flag_ping:
    import beepy

UNITIALIZED_ACTION = -100

class TaskAction():
    def __init__(self):
        self.type = Constant.TASK_TYPE_EXPLORE
        self.task_stage = 0
        self.task_stage_num = 1
        self.task_robot_id = [[1, 2]]
        self.task_action_type = [[Constant.ACTION_STATE_EXPLORE, Constant.ACTION_STATE_EXPLORE]]
        self.task_action_id = [[UNITIALIZED_ACTION, UNITIALIZED_ACTION]]
        self.task_action_success = [[False, False]]
        self.sub_region = []
        self.block_ids = []
        self.robot_block_ids = [[], []]
        self.task_time = -1.0
        self.task_action_time = [[-1.0, -1.0]]
        self.task_depend = []
        self.task_success = False
        self.next_task_id = -1
        self.goal_pose = [0.0, 0.0]
    
    def toDict(self):
        temp_dict = {}
        temp_dict['type'] = self.type
        temp_dict['task_stage_num'] = self.task_stage_num
        temp_dict['task_robot_id'] = self.task_robot_id[0]
        temp_dict['task_action_type'] = self.task_action_type[0]
        temp_dict['task_action_id'] = self.task_action_id[0]
        temp_dict['task_action_success'] = self.task_action_success[0]
        # temp_dict['sub_region'] = self.sub_region
        temp_dict['block_ids'] = self.block_ids
        temp_dict['robot_block_ids'] = self.robot_block_ids
        temp_dict['task_start_time'] = self.task_start_time
        temp_dict['task_end_time'] = self.task_end_time
        temp_dict['task_action_time'] = self.task_action_time[0]
        temp_dict['task_duration'] = self.task_end_time - self.task_start_time
        temp_dict['action_duration'] = [temp_time - self.task_start_time for temp_time in self.task_action_time[0]]
        temp_dict['goal_pose'] = self.goal_pose
        # Count the picked block numbers
        temp_inside_num = len(self.task_robot_id[0])
        temp_robot_large_blocks = np.zeros((temp_inside_num), dtype=int)
        temp_robot_small_blocks = np.zeros((temp_inside_num), dtype=int)
        for i_inside in range(temp_inside_num):
            for temp_block_type in self.robot_block_types[i_inside]:
                if temp_block_type == Constant.BLOCK_LIGHT:
                    temp_robot_small_blocks[i_inside] += 1
                else:
                    temp_robot_large_blocks[i_inside] += 1
        temp_dict['robot_small_blocks'] = temp_robot_small_blocks.tolist()
        temp_dict['robot_large_blocks'] = temp_robot_large_blocks.tolist()
        return temp_dict

    def setExplore(self, robot_id, region, task_depend = []):
        robot_num = len(robot_id)
        self.type = Constant.TASK_TYPE_EXPLORE
        self.task_stage = 0
        self.task_stage_num = 1
        self.task_robot_id = [[temp_id for temp_id in robot_id]]
        self.task_action_type = [[Constant.ACTION_STATE_EXPLORE]*robot_num]
        self.task_action_id = [[UNITIALIZED_ACTION]*robot_num]
        self.task_action_success = [[False]*robot_num]
        self.sub_region = self.processRegion(robot_num, region)
        self.block_ids = []
        self.robot_block_ids = [[] for i_inside in range(robot_num)]
        self.robot_block_types = [[] for i_inside in range(robot_num)]
        # print('self.sub_region = ', self.sub_region)
        self.task_start_time = rospy.Time.now().to_sec()
        self.task_end_time = -1.0
        self.task_action_time = [[-1.0]*robot_num]
        self.task_depend = task_depend
        self.next_task_id = -1

    def setPick(self, robot_id, block_ids, task_depend = []):
        robot_num = len(robot_id)
        self.type = Constant.TASK_TYPE_PICK
        self.task_stage = 0
        self.task_stage_num = 1
        self.task_robot_id = [[temp_id for temp_id in robot_id]]
        self.task_action_type = [[Constant.ACTION_STATE_PICK]*robot_num]
        self.task_action_id = [[UNITIALIZED_ACTION]*robot_num]
        self.task_action_success = [[True]*robot_num]
        self.sub_region = []
        self.block_ids = block_ids
        self.robot_block_ids = [[] for i_inside in range(robot_num)]
        self.robot_block_types = [[] for i_inside in range(robot_num)]
        self.task_start_time = rospy.Time.now().to_sec()
        self.task_end_time = -1.0
        self.task_action_time = [[-1.0]*robot_num]
        self.task_depend = task_depend
        self.next_task_id = -1
        self.task_success = False

    def setMove(self, robot_id, goal_pose, task_depend = []):
        robot_num = len(robot_id)
        self.type = Constant.TASK_TYPE_MOVE
        self.task_stage = 0
        self.task_stage_num = 1
        self.task_robot_id = [[temp_id for temp_id in robot_id]]
        self.task_action_type = [[Constant.ACTION_STATE_MOVE]*robot_num]
        self.task_action_id = [[UNITIALIZED_ACTION]*robot_num]
        self.task_action_success = [[False]*robot_num]
        self.sub_region = []
        self.block_ids = []
        self.robot_block_ids = [[] for i_inside in range(robot_num)]
        self.robot_block_types = [[] for i_inside in range(robot_num)]
        self.task_start_time = rospy.Time.now().to_sec()
        self.task_end_time = -1.0
        self.task_action_time = [[-1.0]*robot_num]
        self.task_depend = task_depend
        self.next_task_id = -1
        self.goal_pose = [goal_pose[0], goal_pose[1]]

    def setFind(self, robot_id, block_ids, region, next_task_id, task_depend = []):
        robot_num = len(robot_id)
        self.type = Constant.TASK_TYPE_FIND
        self.task_stage = 0
        self.task_stage_num = 1
        self.task_robot_id = [[temp_id for temp_id in robot_id]]
        self.task_action_type = [[Constant.ACTION_STATE_FIND]*robot_num]
        self.task_action_id = [[UNITIALIZED_ACTION]*robot_num]
        self.task_action_success = [[False]*robot_num]
        self.sub_region = self.processRegion(robot_num, region)
        self.block_ids = [id for id in block_ids]
        self.robot_block_ids = [[block_ids[0]] for i_inside in range(robot_num)]
        self.robot_block_types = [[] for i_inside in range(robot_num)]
        self.task_start_time = rospy.Time.now().to_sec()
        self.task_end_time = -1.0
        self.task_action_time = [[-1.0]*robot_num]
        self.task_depend = task_depend
        self.next_task_id = next_task_id

    def success(self):
        if self.task_success:
            return 1
        temp_task_success = True
        for temp_action_success in self.task_action_success[self.task_stage]:
            temp_task_success &= temp_action_success
        if temp_task_success:
            if self.type == Constant.TASK_TYPE_PICK:
                if self.block_ids:
                    return 0
            if self.task_end_time < 0.0:
                self.task_end_time = rospy.Time.now().to_sec()
            self.task_success = True
            return 2
        else:
            return 0

    def check_action(self, robot_id, action_id, action_id_state, allocator):
        for i_inside in range(len(self.task_robot_id[self.task_stage])):
            robot_id_inside = self.task_robot_id[self.task_stage][i_inside]
            if robot_id != robot_id_inside:
                continue
            action_id_inside = self.task_action_id[self.task_stage][i_inside]
            if action_id == action_id_inside and action_id_state != Constant.ACTION_ID_ONGOING:
                self.task_action_success[self.task_stage][i_inside] = True
                if self.task_action_time[self.task_stage][i_inside] < 0.0:
                    self.task_action_time[self.task_stage][i_inside] = rospy.Time.now().to_sec()
                # print("Completed", robot_id, action_id, action_id_state)
            if action_id == action_id_inside and action_id_state == Constant.ACTION_ID_FIND:
                self.force_idle(allocator)
                time.sleep(3.0)
                for i_inside2 in range(len(self.task_robot_id[self.task_stage])):
                    self.task_action_success[self.task_stage][i_inside2] = True
                    if self.task_action_time[self.task_stage][i_inside2] < 0.0:
                        self.task_action_time[self.task_stage][i_inside2] = rospy.Time.now().to_sec()
                break
            # print("ACTION_ID_FIND", self.task_action_success)

    def force_idle(self, allocator):
        for i_inside in range(len(self.task_robot_id[self.task_stage])):
            robot_id = self.task_robot_id[self.task_stage][i_inside]
            i_robot = allocator.id2int[robot_id]
            if self.task_action_success[self.task_stage][i_inside] and (not allocator.robot_feed_ready[i_robot]):
                action_type = Constant.ACTION_STATE_FORCEIDLE
                next_action_id = allocator.action_id
                allocator.action_id += 1
                goal_pose = []
                goal_region = []
                block_ids = []
                one_message = [i_robot, robot_id, action_type, next_action_id, goal_pose, goal_region, block_ids]
                allocator.action_mesasge_set[next_action_id] = one_message
                allocator.publishAction(i_robot, robot_id, action_type, next_action_id, goal_pose, goal_region, block_ids)
                print('Forceidle')

    def process(self, allocator):
        print('task_action_success', allocator.task_id, self.task_action_success[self.task_stage], 'Ready:', allocator.robot_feed_ready)
        for i_inside in range(len(self.task_robot_id[self.task_stage])):
            robot_id = self.task_robot_id[self.task_stage][i_inside]
            i_robot = allocator.id2int[robot_id]
            if robot_id == 0:
                # print('process', self.task_robot_id[self.task_stage], i_robot, robot_id)
                continue
            # print('Robot', robot_id, ' tag 1', 'self.block_ids = ', self.block_ids)
            if not allocator.robot_feed_ready[i_robot]:
                continue
            action_type = self.task_action_type[self.task_stage][i_inside]
            # Construct a new action
            # print('Robot', robot_id, 'tag 2')
            if self.type == Constant.TASK_TYPE_EXPLORE:
                if self.task_action_success[self.task_stage][i_inside]:
                    continue
                goal_pose = np.zeros(2)
                goal_region = self.sub_region[i_inside]
                block_ids = []
            elif self.type == Constant.TASK_TYPE_MOVE:
                goal_pose = self.goal_pose
                goal_region = []
                block_ids = []
            elif self.type == Constant.TASK_TYPE_PICK:
                print('task', allocator.task_id, 'robot', robot_id, 'start_block_ids = ', self.block_ids)
                # print('Robot', robot_id, 'tag 3')
                if not (allocator.robot_type[i_robot] in [Constant.ROB_TYPE_LARGE, Constant.ROB_TYPE_LARGE_HEAVY]):
                    continue
                # print('Robot', robot_id, 'tag 4')
                if not self.block_ids:
                    continue
                # print('Robot', robot_id, 'tag 5')
                if allocator.robot_type[i_robot] == Constant.ROB_TYPE_LARGE:
                    flag_get_one = False
                    for temp_id in range(len(self.block_ids)):
                        temp_block = self.block_ids[temp_id]
                        if allocator.block_type[temp_block] == Constant.BLOCK_LIGHT:
                            self.block_ids.pop(temp_id)
                            flag_get_one = True
                            break
                    if not flag_get_one:
                        continue
                else: # allocator.robot_type[i_robot] == Constant.ROB_TYPE_LARGE_HEAVY
                    flag_get_one = False
                    for temp_id in range(len(self.block_ids)):
                        temp_block = self.block_ids[temp_id]
                        if allocator.block_type[temp_block] == Constant.BLOCK_HEAVY:
                            self.block_ids.pop(temp_id)
                            flag_get_one = True
                            break
                    if not flag_get_one:
                        for temp_id in range(len(self.block_ids)):
                            temp_block = self.block_ids[temp_id]
                            if allocator.block_type[temp_block] == Constant.BLOCK_LIGHT:
                                self.block_ids.pop(temp_id)
                                flag_get_one = True
                                break
                    # temp_block = self.block_ids.pop()
                # print('task', allocator.task_id, 'robot', robot_id, 'end_block_ids = ', self.block_ids)
                self.task_action_success[self.task_stage][i_inside] = False
                self.task_action_time[self.task_stage][i_inside] = -1.0
                # print('Robot', robot_id, 'tag 6', 'self.robot_block_ids = ', self.robot_block_ids)
                self.robot_block_ids[i_inside].append(temp_block)
                self.robot_block_types[i_inside].append(allocator.block_type[temp_block])
                goal_pose = np.zeros(2)
                goal_region = []
                block_ids = [temp_block]
                # print('Robot', robot_id, 'tag 7')
            elif self.type == Constant.TASK_TYPE_FIND:
                if self.task_action_success[self.task_stage][i_inside]:
                    continue
                goal_pose = np.zeros(2)
                goal_region = self.sub_region[i_inside]
                block_ids = self.robot_block_ids[i_inside]
            else:
                print('Warning: type = ', self.type)
            # Assign a new action_id
            if allocator.robot_next_action_id[i_robot] <= allocator.robot_feed_action_id[i_robot]: # < is not possible, we are testing whether ==
                allocator.robot_next_action_id[i_robot] = allocator.action_id
                self.task_action_id[self.task_stage][i_inside] = allocator.action_id
                # print("allocator.action_id = ", allocator.action_id)
                allocator.action_id += 1
            next_action_id = allocator.robot_next_action_id[i_robot]
            # Publish the new action
            # self.robot_mesasge_id[i_robot] = next_action_id
            one_message = [i_robot, robot_id, action_type, next_action_id, goal_pose, goal_region, block_ids]
            allocator.action_mesasge_set[next_action_id] = one_message
            allocator.publishAction(i_robot, robot_id, action_type, next_action_id, goal_pose, goal_region, block_ids)
            allocator.robot_feed_ready[i_robot] = False
            print("publish new task_id =", allocator.task_id, "next_action =", allocator.robot_next_action_id, "feed_action =", allocator.robot_feed_action_id, "allocator.robot_feed_ready =", allocator.robot_feed_ready, 'self.task_action_id[self.task_stage]', self.task_action_id[self.task_stage])

    def processRegion(self, robot_num, region, flag_x = True):
        sub_region = []
        region_x_min = region[0]
        region_y_min = region[1]
        region_x_max = region[2]
        region_y_max = region[3]
        if robot_num <= 3 or robot_num >= 5:
            if not flag_x:
                region_x_array = np.linspace(region_x_min, region_x_max, robot_num+1)
                for i_inside in range(robot_num):
                    x1 = region_x_array[i_inside]
                    x2 = region_x_array[i_inside+1]
                    sub_region.append([x1, region_y_min, x2, region_y_max])
            else:
                region_y_array = np.linspace(region_y_min, region_y_max, robot_num+1)
                for i_inside in range(robot_num):
                    y1 = region_y_array[i_inside]
                    y2 = region_y_array[i_inside+1]
                    # sub_region.insert(0, [region_x_min, y1, region_x_max, y2])
                    sub_region.append([region_x_min, y1, region_x_max, y2])
        elif robot_num == 4:
            region_x_mid = 0.5 * (region_x_min + region_x_max)
            region_y_mid = 0.5 * (region_y_min + region_y_max)
            sub_region.append([region_x_min, region_y_mid, region_x_mid, region_y_max])
            sub_region.append([region_x_min, region_y_min, region_x_mid, region_y_mid])
            sub_region.append([region_x_mid, region_y_mid, region_x_max, region_y_max])
            sub_region.append([region_x_mid, region_y_min, region_x_max, region_y_mid])
        else: # robot_num == 3:
            if not flag_x:
                region_x_mid = 0.5 * (region_x_min + region_x_max)
                region_y_mid = 2/3 * region_y_min + 1/3 * region_y_max
                sub_region.append([region_x_min, region_y_mid, region_x_mid, region_y_max])
                sub_region.append([region_x_mid, region_y_mid, region_x_max, region_y_max])
                sub_region.append([region_x_min, region_y_min, region_x_max, region_y_mid])
            else:
                region_y_mid = 0.5 * (region_y_min + region_y_max)
                region_x_mid = 2/3 * region_x_min + 1/3 * region_x_max
                sub_region.append([region_x_min, region_y_min, region_x_mid, region_y_max])
                sub_region.append([region_x_mid, region_y_min, region_x_max, region_y_mid])
                sub_region.append([region_x_mid, region_y_mid, region_x_max, region_y_max])
        return sub_region


class TaskAllocator():
    def __init__(self, enable_visulization, robot_num, robot_type_list, block_type_list, file_name = None):
        self.grid_map = GridMap()
        self.grid_map.region_set = True

        self.robot_num = robot_num
        self.robot_feed_ready = np.ones(robot_num, dtype=bool)
        self.robot_feed_action_id = np.ones(robot_num, dtype=np.int64) * UNITIALIZED_ACTION
        self.robot_next_action_id = np.ones(robot_num, dtype=np.int64) * UNITIALIZED_ACTION
        self.int2id = np.arange(1, robot_num+1)
        self.id2int = np.arange(-1, robot_num)
        self.robot_type = robot_type_list

        self.action_id = 0
        self.action_pub = []

        self.region_list =  np.array([])
        self.task_num = 0
        self.task_id = 0
        self.task_state = []
        self.task_success = np.zeros(self.task_num, dtype=bool)
        self.block_type = block_type_list
        self.world_setup_dict = None

        self.flag_update_pose = True
        if self.flag_update_pose:
            self.block_num = len(block_type_list)
            self.block_pose = np.zeros((self.block_num, 2), dtype=np.float64)
            self.robot_pose = np.zeros((self.robot_num, 2), dtype=np.float64)
            self.gazebo_srv = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            self.gazebo_req = GetModelStateRequest()
            pose_update_rate = rospy.Duration(0.05)
            rospy.Timer(pose_update_rate, self.poseUpdateCallback)

        self.file_name = file_name
        self.file_save_id = -999

        self.action_mesasge_set = {}

        allocation_rate = rospy.Duration(0.1)
        rospy.Timer(allocation_rate, self.allocationCallback)

        for i_robot in range(self.robot_num):
            robot_id = self.int2id[i_robot]
            rospy.Subscriber("/robot" + str(robot_id) + "/action_feedback", RobotActionFeedback, self.actionFeedbackCallback)
            temp_action_pub = rospy.Publisher("/robot" + str(robot_id) + "/action", RobotAction, queue_size=10)
            self.action_pub.append(temp_action_pub)

        self.enable_visulization = enable_visulization
        if self.enable_visulization:
            visulization_rate = rospy.Duration(0.5)
            self.region_pub = rospy.Publisher('/region_marker', MarkerArray, queue_size=10)
            self.explored_pub = rospy.Publisher('/explored', MarkerArray, queue_size=10)
            rospy.Timer(visulization_rate, self.visulizationCallback)
            for i_robot in range(self.robot_num):
                robot_id = self.int2id[i_robot]
                rospy.Subscriber("/robot" + str(robot_id) + "/visualization", RobotVisualization, self.RobotVisualCallback)

    def setSetupDict(self, world_setup_dict):
        self.world_setup_dict = world_setup_dict

    def saveResult(self):
        if not self.file_name:
            return
        result_dict = {}
        for temp_task_id in range(self.task_num):
            task_dict = self.task_state[temp_task_id].toDict()
            result_dict['task' + str(temp_task_id)] = task_dict
        if self.world_setup_dict:
            result_dict['world_setup'] = self.world_setup_dict
        with open(self.file_name, 'w') as yaml_file:
            yaml.dump(result_dict, yaml_file, default_flow_style=None)

    def processNextTask(self, this_task:TaskAction, next_task:TaskAction):
        if this_task.type == Constant.TASK_TYPE_FIND:
            if self.flag_update_pose:
                # Choose the closest robot to pick the block
                i_block = this_task.block_ids[0]
                i_robot = self.id2int[this_task.task_robot_id[0]].tolist()
                block_pose = self.block_pose[i_block, :].reshape(1, -1)
                robot_pose = self.robot_pose[i_robot, :]
                temp_dist = cdist(block_pose, robot_pose)[0]
                i_sort = np.argsort(temp_dist) # np.sort(temp_dist) == temp_dist[i_sort]
                robot_id_list = np.array(this_task.task_robot_id[0])[i_sort].tolist()
            else:
                robot_id_list = this_task.task_robot_id[0]
            pick_robot_id = -1
            for robot_id in robot_id_list:
                i_robot = self.id2int[robot_id]
                if self.robot_type[i_robot] == Constant.ROB_TYPE_LARGE:
                    pick_robot_id = robot_id
                    break
            if pick_robot_id == -1:
                for robot_id in robot_id_list:
                    i_robot = self.id2int[robot_id]
                    if self.robot_type[i_robot] == Constant.ROB_TYPE_LARGE_HEAVY:
                        pick_robot_id = robot_id
                        break
            next_task.setPick(robot_id=[pick_robot_id], block_ids=[i_block])

    def allocationCallback(self, msg=None):
        if len(self.task_state) == 0:
            return
        print('General task state', self.task_success)
        flag_all_success = True
        for i_task in range(self.task_num):
            # Check if the task has been completed
            temp_success = self.task_state[i_task].success()
            self.task_success[i_task] = self.task_state[i_task].task_success
            
            if temp_success == 1:
                continue
            elif temp_success == 2:
                self.saveResult()
                if self.task_state[i_task].type == Constant.TASK_TYPE_FIND:
                    next_task_id = self.task_state[i_task].next_task_id
                    self.processNextTask(self.task_state[i_task], self.task_state[next_task_id])
                continue
            flag_all_success = False
            # Check if the dependency has been completed
            flag_depend = True
            for j_task in self.task_state[i_task].task_depend:
                if not self.task_state[j_task].task_success:
                    flag_depend = False
                    break
            if flag_depend:
                self.task_id = i_task
                self.task_state[i_task].process(self)
        if flag_ping and flag_all_success:
            beepy.beep(sound="ping")

    def publishAction(self, i_robot, robot_id, action_type, action_id, goal_pose, goal_region, block_ids):
        action_msg = RobotAction()
        action_msg.robot_id = robot_id
        action_msg.action_type = action_type
        action_msg.action_id = action_id
        action_msg.block_ids = block_ids
        action_msg.goal_pose = goal_pose
        action_msg.goal_region = goal_region
        self.action_pub[i_robot].publish(action_msg)
        print('action_msg = ', action_msg)

    def actionFeedbackCallback(self, feedback_msg):
        if len(self.task_state) == 0:
            return
        i_robot = self.id2int[feedback_msg.robot_id]
        self.robot_feed_action_id[i_robot] = feedback_msg.action_id
        print('feedback', 'robot_id', feedback_msg.robot_id, 'next_action_id', self.robot_next_action_id[i_robot], 'feed_action_id', self.robot_feed_action_id[i_robot])
        if self.robot_next_action_id[i_robot] > self.robot_feed_action_id[i_robot]:
            i_robot0, robot_id, action_type, next_action_id, goal_pose, goal_region, block_ids = self.action_mesasge_set[self.robot_next_action_id[i_robot]]
            self.robot_feed_ready[i_robot] = False
            self.publishAction(i_robot, robot_id, action_type, next_action_id, goal_pose, goal_region, block_ids)
            # print("publish old task_id =", i_task, "next_action =", self.robot_next_action_id, "feed_action =", self.robot_feed_action_id, "allocator.robot_feed_ready =", self.robot_feed_ready)
        else:
            self.robot_next_action_id[i_robot] = self.robot_feed_action_id[i_robot]
            self.robot_feed_ready[i_robot] = (feedback_msg.action_id_state != Constant.ACTION_ID_ONGOING)
        for i_task in range(self.task_num):
            if self.task_success[i_task]:
                continue
            self.task_state[i_task].check_action(feedback_msg.robot_id, feedback_msg.action_id, feedback_msg.action_id_state, self)

    def poseUpdateCallback(self, msg=None):
        for i_robot in range(self.robot_num):
            robot_id = self.int2id[i_robot]
            self.gazebo_req.model_name = "robot" + str(robot_id)
            gazebo_result = self.gazebo_srv(self.gazebo_req)
            self.robot_pose[i_robot, 0] = gazebo_result.pose.position.x
            self.robot_pose[i_robot, 1] = gazebo_result.pose.position.y
        for i_block in range(self.block_num):
            self.gazebo_req.model_name = "block" + str(i_block)
            gazebo_result = self.gazebo_srv(self.gazebo_req)
            self.block_pose[i_block, 0] = gazebo_result.pose.position.x
            self.block_pose[i_block, 1] = gazebo_result.pose.position.y

    def visulizationCallback(self, msg=None):
        region_marker_array = MarkerArray()
        point_x = self.region_list[:, 0]
        point_y = self.region_list[:, 1]
        d_x     = self.region_list[:, 2] - self.region_list[:, 0]
        d_y     = self.region_list[:, 3] - self.region_list[:, 1]
        region_marker_array = Visualization.getSquareMarker(point_x, point_y, d_x, d_y)
        self.region_pub.publish(region_marker_array)

        explored_x, explored_y = self.grid_map.exploredGoals(flag_index=False)
        # print('explored_x = ', len(explored_x))
        explored_marker_array = Visualization.getExploredMarker(explored_x, explored_y)
        self.explored_pub.publish(explored_marker_array)

    def RobotVisualCallback(self, msg):
        self.grid_map.forceExplored(msg.explored_x, msg.explored_y)

    def setTask(self, flag_task = 2):
        if flag_task == 1:
            # Agents explore a big area
            self.task_num = 1
            self.task_state = [TaskAction()]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            self.region_list = np.array([[-0.8, -0.8, 4.8, 4.8]]) # xmin, ymin, xmax, ymax
            temp_task_robot = self.int2id[:len(self.robot_type)].tolist()
            self.task_state[0].setExplore(temp_task_robot, self.region_list[0, :4])
        elif flag_task in [2,3,4]:
            # Agent pickup blocks
            self.region_list =  np.zeros((0, 4))
            self.task_num = 1
            self.task_state = [TaskAction()]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            temp_task_robot = self.int2id[:len(self.robot_type)].tolist()
            temp_block_ids = list(range(len(self.block_type)))
            self.task_state[0].setPick(robot_id=temp_task_robot, block_ids=temp_block_ids)
        elif flag_task == 5:
            # Find the block and then pickup the block
            self.task_num = 2
            self.task_state = [TaskAction(), TaskAction()]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            self.region_list = np.array([[-0.8, -0.8, 4.8, 4.8]]) # xmin, ymin, xmax, ymax
            temp_task_robot = self.int2id[:len(self.robot_type)].tolist()
            temp_block_ids = [0]
            self.task_state[0].setFind(robot_id=temp_task_robot, block_ids=[0], region=self.region_list[0], next_task_id=1) # THe block ids should be a list contain a single block
            self.task_state[1].setPick(robot_id=[0], block_ids=[0], task_depend = [0])
        elif flag_task == 10:
            # Find the block and then pickup the block
            self.task_num = 12
            self.task_state = [TaskAction() for i_task in range(self.task_num)]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            self.region_list = np.array([[7.36, -4.24, 11.47, 1-0.76], [5.00, -9.00, 8.00, -5.00]]) # xmin, ymin, xmax, ymax

            # 5
            self.task_state[0].setFind(robot_id=[1, 2], block_ids=[0], region=self.region_list[0], next_task_id=1, task_depend = [2,5]) # THe block ids should be a list contain a single block
            self.task_state[1].setPick(robot_id=[0],    block_ids=[0], task_depend = [0])
            
            # 1
            self.task_state[5].setExplore(robot_id=[1], region=self.region_list[1], task_depend = [])
            # 2
            self.task_state[2].setPick(robot_id=[2, 3], block_ids=[1,2,3,4], task_depend = [])

            self.task_state[3].setPick(robot_id=[4,5,6], block_ids=[5,6,7,8], task_depend = [])
            # self.task_state[3].setPick(robot_id=[4,5,6], block_ids=[], task_depend = [])
            self.task_state[4].setPick(robot_id=[4,5,6], block_ids=[9,10,11,12], task_depend = [3])

            pre_robot_x_list = [0.0, 1.20,  1.20,  1.20, -0.20, -0.20, -0.20]
            pre_robot_y_list = [0.0, 2.30,  0.30, -1.70,  2.30,  0.30, -1.70]
            self.task_state[6].setMove(robot_id=[1], goal_pose = [pre_robot_x_list[1], pre_robot_y_list[1]], task_depend = [1])
            self.task_state[7].setMove(robot_id=[2], goal_pose = [pre_robot_x_list[2], pre_robot_y_list[2]], task_depend = [1])
            self.task_state[8].setMove(robot_id=[3], goal_pose = [pre_robot_x_list[3], pre_robot_y_list[3]], task_depend = [2])
            self.task_state[9].setMove(robot_id=[4], goal_pose = [pre_robot_x_list[4], pre_robot_y_list[4]], task_depend = [4])
            self.task_state[10].setMove(robot_id=[5], goal_pose = [pre_robot_x_list[5], pre_robot_y_list[5]], task_depend = [4])
            self.task_state[11].setMove(robot_id=[6], goal_pose = [pre_robot_x_list[6], pre_robot_y_list[6]], task_depend = [4])
            # self.task_state[12].setMove(robot_id=[2], goal_pose = [8.15, -0.52], task_depend = [2])
        elif flag_task == 11:
            # Find the block and then pickup the block
            self.task_num = 13
            self.task_state = [TaskAction() for i_task in range(self.task_num)]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            self.region_list = np.array([[2.5, -4.8, 4.8, -0.5], [-3.0, -4.5, 0.0, -3.5]]) # xmin, ymin, xmax, ymax

            # 5
            self.task_state[0].setFind(robot_id=[1, 2], block_ids=[0], region=self.region_list[0], next_task_id=1, task_depend = [2,6]) # THe block ids should be a list contain a single block
            self.task_state[1].setPick(robot_id=[0],    block_ids=[0], task_depend = [0])

            
            # 1
            self.task_state[5].setExplore(robot_id=[1], region=self.region_list[1], task_depend = [])
            # 2
            self.task_state[2].setPick(robot_id=[2, 3], block_ids=[1,2,3,4], task_depend = [])

            self.task_state[3].setPick(robot_id=[4,5,6], block_ids=[5,6,7,8], task_depend = [])
            self.task_state[4].setPick(robot_id=[4,5,6], block_ids=[9,10,11,12], task_depend = [3])

            pre_robot_x_list = [0.0, -4.0,   -4.0,   -4.0,  -4.0,  -4.0,  -4.0]
            pre_robot_y_list = [0.0, -4.0,  -2.5, -1.0,  1.0,  2.5, 4.0]
            self.task_state[6].setMove(robot_id=[2], goal_pose = [2.5, -1.0], task_depend = [2])
            self.task_state[7].setMove(robot_id=[1], goal_pose = [pre_robot_x_list[1], pre_robot_y_list[1]], task_depend = [1])
            self.task_state[8].setMove(robot_id=[2], goal_pose = [pre_robot_x_list[2], pre_robot_y_list[2]], task_depend = [1])
            self.task_state[9].setMove(robot_id=[3], goal_pose = [pre_robot_x_list[3], pre_robot_y_list[3]], task_depend = [2])
            self.task_state[10].setMove(robot_id=[4], goal_pose = [pre_robot_x_list[4], pre_robot_y_list[4]], task_depend = [4])
            self.task_state[11].setMove(robot_id=[5], goal_pose = [pre_robot_x_list[5], pre_robot_y_list[5]], task_depend = [4])
            self.task_state[12].setMove(robot_id=[6], goal_pose = [pre_robot_x_list[6], pre_robot_y_list[6]], task_depend = [4])
        if flag_task == 20:
            self.region_list =  np.array([[-1.54443586, -0.32825109, -0.56806839,  0.55004281],
                                        [0.5236879,  -1.588027,    1.62432063, -0.63875961],
                                        [0.68345731,  0.61214435,  1.59769261,  1.54366839]])
            self.task_num = 3
            self.task_state = [TaskAction(), TaskAction(), TaskAction()]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            for i_task in range(self.task_num):
                self.task_state[i_task].setExplore([1, 2], self.region_list[i_task, :4])
        elif flag_task == 21:
            self.region_list =  np.array([[-2.68345731,  0.61214435,  -0.5,  2.54366839]])
            self.task_num = 2
            self.task_state = [TaskAction(), TaskAction()]
            self.task_success = np.zeros(self.task_num, dtype=bool)
            self.task_state[0].setExplore([1, 2], self.region_list[0, :4])
            self.task_state[1].setPick(robot_id=[1, 2], block_ids=[0,1,2], task_depend = [0])
        elif flag_task == 22:
            self.region_list =  np.zeros((0, 4))
            self.task_state = [TaskAction()]
            self.task_num = len(self.task_state)
            self.task_success = np.zeros(self.task_num, dtype=bool)
            self.task_state[0].setMove(robot_id=[1], goal_pose = [3.5, -1.0], task_depend = [])
