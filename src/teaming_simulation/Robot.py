import rospy
import tf
import math
import actionlib
import numpy as np
from sensor_msgs.msg import LaserScan
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction
from geometry_msgs.msg import Quaternion
from tf.transformations import euler_from_quaternion, quaternion_from_euler, quaternion_matrix
from scipy.spatial.distance import cdist
from teaming_simulation.msg import RobotVisualization, RobotAction, RobotActionFeedback, BrodcastPose
from teaming_simulation.GridMap import GridMap
from teaming_simulation.Constant import Constant
from teaming_simulation.Arm import Arm
from gazebo_ros_link_attacher.srv import Attach, AttachRequest, AttachResponse
from gazebo_msgs.srv import GetModelState, GetModelStateRequest

class Robot:
    def __init__(self, priority, block_num, robot_id, robot_type = Constant.ROB_TYPE_SMALL, enable_visulization = False):
        '''
            Constructor for Robot class
            Parameters:
                    robot_id (int): a integer represent the current robot index
                    priority (int): priority of the robot
            Returns:
                    None
        '''
        self.flag_gtpose = True

        self.robot_id = robot_id
        self.robot_type = robot_type
        self.initialized = False
        self.curr_pose = None
        self.curr_T = None
        self.slam_pose = None
        self.slam_T = None
        self.gazebo_pose = None
        self.gazebo_T = None
        self.offset_pose = np.zeros((3), dtype=np.float64)
        self.priority = priority
        self.goal = np.zeros(2)
        self.goal_angle = -100.0

        self.block_T = [None for i_block in range(block_num)]
        self.flag_attach = [False for i_block in range(block_num)]
        self.pick_block_id = None
        self.pick_block_ids = []
        self.arm = Arm(self.robot_id)
        self.gripper_states = [[1.0, 1.0], [-1.0, -1.0], [-1.0, -1.0], [-1.0, -1.0], [1.0, 1.0], [1.0, 1.0]]
        self.joint_angles = [[0.0, 0.6, 0.4, 0.5], [0.0, 0.6, 0.4, 0.5], [0.0, -0.5, 0.5, 0.5], None, None, [0.0, 0.0, 0.0, 0.0]]
        self.detach_angles = [[-70*Constant.DEG2RAD, -56*Constant.DEG2RAD, 51*Constant.DEG2RAD, 68*Constant.DEG2RAD],
                                [70*Constant.DEG2RAD, -56*Constant.DEG2RAD, 51*Constant.DEG2RAD, 68*Constant.DEG2RAD],
                                [-120*Constant.DEG2RAD, -56*Constant.DEG2RAD, 51*Constant.DEG2RAD, 68*Constant.DEG2RAD],
                                [120*Constant.DEG2RAD, -56*Constant.DEG2RAD, 51*Constant.DEG2RAD, 68*Constant.DEG2RAD],
                                [-150*Constant.DEG2RAD, -56*Constant.DEG2RAD, 51*Constant.DEG2RAD, 68*Constant.DEG2RAD],
                                [150*Constant.DEG2RAD, -56*Constant.DEG2RAD, 51*Constant.DEG2RAD, 68*Constant.DEG2RAD]]
        self.detach_num = len(self.detach_angles)
        self.picked_num = 0

        self.joint_angle_id = -1
        self.joint_angle_attach_id = 2
        self.joint_angle_detach_id = 4
        self.prev_pick_message_time = -10.0
        self.prev_goal_time = -10.0
        
        self.attach_srv = rospy.ServiceProxy('/link_attacher_node/attach', Attach)
        self.attach_srv.wait_for_service()
        self.detach_srv = rospy.ServiceProxy('/link_attacher_node/detach', Attach)
        self.detach_srv.wait_for_service()
        self.flag_picking = False

        self.move_state = Constant.MOVE_STATE_IDLE
        # self.frame = "robot" + str(robot_id) + "/base_scan"
        self.base = "robot" + str(robot_id) + "/base_footprint" 
        self.setRobotParam(self.robot_type)

        self.action_state = Constant.ACTION_STATE_IDLE
        self.next_action_msg = RobotAction()
        self.next_action_received = False

        self.action_id = -1
        self.action_id_list = []
        self.action_id_state = Constant.ACTION_ID_SUCCESS

        self.grid_map = GridMap()

        self.client = actionlib.SimpleActionClient("robot" + str(robot_id) + '/move_base', MoveBaseAction)
        self.listener = tf.TransformListener()

        #Subscribe to lidar scan 
        # rospy.Subscriber("robot" + str(robot_id) + "/scan", LaserScan, self.scanCallBack)
        
        # Thread for send robot goal
        goal_rate = rospy.Duration(0.5)
        rospy.Timer(goal_rate,self.sendGoal_callback)

        pose_rate = rospy.Duration(0.05)
        rospy.Timer(pose_rate, self.updatePose)

        action_rate = rospy.Duration(0.05)
        rospy.Timer(action_rate, self.actionCallback)

        rospy.Subscriber("robot" + str(robot_id) + "/action", RobotAction, self.nextActionCallback)

        # Subscribe to lidar scan 
        rospy.Subscriber("robot" + str(robot_id) + "/scan", LaserScan, self.scanCallback)

        rospy.Subscriber("robot_poses", BrodcastPose, self.priorityCallback)

        self.action_feedback_pub = rospy.Publisher("robot" + str(robot_id) + "/action_feedback", RobotActionFeedback, queue_size=10)

        self.enable_visulization = enable_visulization
        if self.enable_visulization:
            visulization_rate = rospy.Duration(1.0)
            self.visulization_pub = rospy.Publisher("robot" + str(robot_id) + "/visualization", RobotVisualization, queue_size=10)
            rospy.Timer(visulization_rate, self.visulizationCallback)
        
        if self.flag_gtpose:
            self.gazebo_srv = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            self.gazebo_req = GetModelStateRequest()
            self.gazebo_req.model_name = "robot" + str(robot_id)

    def setRobotParam(self, robot_type):
        # The robot's task capabilities are affected by these parameters (there are other factors too)
        if robot_type == Constant.ROB_TYPE_SMALL:
            self.sensor_range = 1.0
            self.pick_message_time_gap = 5.0
        elif robot_type == Constant.ROB_TYPE_SMALL_BROAD:
            self.sensor_range = 1.5
            self.pick_message_time_gap = 5.0
        elif robot_type == Constant.ROB_TYPE_LARGE_NOARM:
            self.sensor_range = 1.0
            self.pick_message_time_gap = 5.0
        elif robot_type == Constant.ROB_TYPE_LARGE:
            self.sensor_range = 1.0
            self.pick_message_time_gap = 2.5
        else: # ROB_TYPE_LARGE_HEAVY
            self.sensor_range = 1.5
            self.pick_message_time_gap = 5.0

    def priorityCallback(self, pose_msg):
        if not self.initialized:
            return
        robot_num = len(pose_msg.robot_id)
        robot_poses = np.array((pose_msg.robot_x, pose_msg.robot_y)).T # size: (robot_num, 2)
        robot_dist = cdist(self.curr_pose[:2].reshape(1,-1), robot_poses).reshape(-1)
        robot_too_close = robot_dist < 0.1
        flag_pause = False
        for i_robot in range(robot_num):
            if self.robot_id == pose_msg.robot_id[i_robot]:
                continue
            if robot_too_close[i_robot] and pose_msg.robot_priority[i_robot] < self.priority:
                flag_pause = True
                break
        if self.move_state == Constant.MOVE_STATE_MOVE:
            if flag_pause:
                self.move_state = Constant.MOVE_STATE_PAUSE
                self.client.cancel_goal()
                print("robot: pause")
        elif self.move_state == Constant.MOVE_STATE_PAUSE:
            if not flag_pause:
                self.move_state = Constant.MOVE_STATE_MOVE
                print("robot: continue")

    def enableRobot(self):
        self.initialized = True

    def cancelAction(self):
        self.cancel_goal()
        self.joint_angle_id = -1
        self.pick_block_id = None
        self.flag_picking = False
        self.pick_block_ids = []
        # self.action_state = Constant.ACTION_STATE_IDLE
        # self.move_state = Constant.MOVE_STATE_IDLE # Handled in self.cancel_goal()

    def nextActionCallback(self, msg):
        if msg.robot_id != self.robot_id or msg.action_id <= self.action_id or msg.action_id in self.action_id_list:
            return False
        self.next_action_msg = msg
        self.next_action_received = True
        print("action_msg = ", msg)
        return True

    def actionCallback(self, msg=None):
        if not self.initialized:
            return
        next_action_state = self.action_state
        if self.action_state == Constant.ACTION_STATE_IDLE:
            if self.next_action_received:
                if self.setAction():
                    self.action_id = self.next_action_msg.action_id
                    self.action_id_state = Constant.ACTION_ID_ONGOING
                    self.next_action_received = False
                    next_action_state = self.next_action_msg.action_type
        elif self.action_state == Constant.ACTION_STATE_MOVE:
            if self.move_state == Constant.MOVE_STATE_IDLE:
                self.action_id_list.append(self.action_id)
                self.action_id_state = Constant.ACTION_ID_SUCCESS
                next_action_state = Constant.ACTION_STATE_IDLE
        elif self.action_state == Constant.ACTION_STATE_EXPLORE:
            if self.explore():
                if self.move_state == Constant.MOVE_STATE_IDLE:
                    self.action_id_list.append(self.action_id)
                    self.action_id_state = Constant.ACTION_ID_SUCCESS
                    next_action_state = Constant.ACTION_STATE_IDLE
        elif self.action_state == Constant.ACTION_STATE_PICK:
            if self.pick():
                self.cancel_goal()
                self.action_id_list.append(self.action_id)
                self.action_id_state = Constant.ACTION_ID_SUCCESS
                next_action_state = Constant.ACTION_STATE_IDLE
        elif self.action_state == Constant.ACTION_STATE_FIND:
            temp_action_id_state = self.find()
            if temp_action_id_state != Constant.ACTION_ID_ONGOING:
                self.action_id_list.append(self.action_id)
                self.action_id_state = temp_action_id_state
                next_action_state = Constant.ACTION_STATE_IDLE
        if self.next_action_received and self.next_action_msg.action_type == Constant.ACTION_STATE_FORCEIDLE:
            self.cancelAction()
            self.action_id = self.next_action_msg.action_id
            self.action_id_state = Constant.ACTION_ID_SUCCESS
            self.next_action_received = False
            next_action_state = Constant.ACTION_STATE_IDLE
            self.action_id_list.append(self.action_id)
        self.action_state = next_action_state
        self.actionFeedback()
        print('robot', self.robot_id, 'initial = ', self.initialized, 'action_state =', self.action_state, ', move_state =', self.move_state, ', action_id =', self.action_id, ', completed =', self.action_id_state, 'action_list', self.action_id_list)

    def setAction(self):
        flag_action_set = False
        if self.next_action_msg.action_type == Constant.ACTION_STATE_MOVE:
            goal = np.array(self.next_action_msg.goal_pose[:2])
            flag_action_set = self.setGoal(goal)
        elif self.next_action_msg.action_type == Constant.ACTION_STATE_EXPLORE:
            region_xy_min = self.next_action_msg.goal_region[0:2]
            region_xy_max = self.next_action_msg.goal_region[2:4]
            flag_action_set = self.setExploreRegion(region_xy_min, region_xy_max)
        elif self.next_action_msg.action_type == Constant.ACTION_STATE_PICK:
            flag_action_set = self.setPick(self.next_action_msg.block_ids)
        elif self.next_action_msg.action_type == Constant.ACTION_STATE_FIND:
            region_xy_min = self.next_action_msg.goal_region[0:2]
            region_xy_max = self.next_action_msg.goal_region[2:4]
            flag_action_set = self.setExploreRegion(region_xy_min, region_xy_max) and self.setPick(self.next_action_msg.block_ids)
        return flag_action_set            


    def actionFeedback(self):
        feedback_msg = RobotActionFeedback()
        feedback_msg.robot_id = self.robot_id
        feedback_msg.action_id = self.action_id
        feedback_msg.action_id_state = self.action_id_state
        self.action_feedback_pub.publish(feedback_msg)

    def sendGoal_callback(self, event=None):
        '''
            Send new goal got the robot by ROS action client 
            Parameters:
                    event: parameter for ros callback function, not neccessary here 
            Returns:
                    None
        '''
        if self.initialized and self.move_state == Constant.MOVE_STATE_MOVE:
            if not self.grid_map.traversable(self.goal[:2]):
                self.move_state = Constant.MOVE_STATE_IDLE
                return
            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = "map"
            goal.target_pose.header.stamp = rospy.Time.now()
            goal.target_pose.pose.position.x = self.goal[0] + self.offset_pose[0]
            goal.target_pose.pose.position.y = self.goal[1] + self.offset_pose[1]

            d_goal = self.goal - self.curr_pose[:2]
            if self.goal_angle > -99.0:
                theta = self.goal_angle
            else:
                theta = math.atan2(d_goal[1], d_goal[0])
            theta += self.offset_pose[2]
            q = quaternion_from_euler(0, 0, theta) # roll, pitch, yaw
            goal.target_pose.pose.orientation = Quaternion(*q) # TODO: Set to zero is questionable

            self.client.send_goal(goal) 
            wait = self.client.wait_for_result()
            # print(self.client.get_result())
            self.move_state = Constant.MOVE_STATE_IDLE
            self.goal_angle = -100.0

    def updatePose(self, event=None):
        '''
            update robot's current location
            Parameters:
                    event: parameter for ros callback function, not neccessary here 

            Returns:
                    None
        '''
        # looking for transformation bewteen robot base frame and map frame (robot's position use map as reference frame)
        try:
            (trans,rot) =self.listener.lookupTransform('/map', self.base, rospy.Time(0))
            slam_euler = euler_from_quaternion(rot)
            self.slam_pose = np.array((trans[0], trans[1], slam_euler[2]))
            # self.slam_pose = np.array((trans[0]+0.7, trans[1]+0.7, slam_euler[2]))
            self.slam_T = quaternion_matrix(rot)
            self.slam_T[:3, 3] = trans

            if self.flag_gtpose:
                gazebo_result = self.gazebo_srv(self.gazebo_req)
                gazebo_trans = (gazebo_result.pose.position.x, gazebo_result.pose.position.y, gazebo_result.pose.position.z)
                gazebo_rot = (gazebo_result.pose.orientation.x, gazebo_result.pose.orientation.y, gazebo_result.pose.orientation.z, gazebo_result.pose.orientation.w)
                gazebo_euler = euler_from_quaternion(gazebo_rot)
                self.gazebo_pose = np.array((gazebo_trans[0], gazebo_trans[1], gazebo_euler[2]))
                self.gazebo_T = quaternion_matrix(gazebo_rot)
                self.gazebo_T[:3, 3] = gazebo_trans
                # print('gazebo_pose:', self.gazebo_pose, 'slam_pose:', self.slam_pose, 'diff', self.slam_pose-self.gazebo_pose)
                self.curr_pose = self.gazebo_pose
                self.curr_T = self.gazebo_T
                self.offset_pose = self.slam_pose - self.gazebo_pose
            else:
                self.curr_pose = self.slam_pose
                self.curr_T = self.slam_T
                self.offset_pose = np.zeros((3), dtype=np.float64)

            for i_block in range(len(self.block_T)):
                block_base = "block" + str(i_block) + "/base_footprint"
                (block_trans, block_rot) = self.listener.lookupTransform('/map', block_base, rospy.Time(0))
                temp_block_T = quaternion_matrix(block_rot)
                temp_block_T[:3, 3] = block_trans
                self.block_T[i_block] = temp_block_T
            if self.grid_map.initialized:
                if not self.initialized:
                    self.enableRobot()
            return True
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            # print(self.robot_id, 'No transform!')
            return False

    def setGoal(self, goal, goal_angle=None):
        '''
            set new goal for the robot
        '''
        if self.move_state != Constant.MOVE_STATE_IDLE:
            return False
        self.goal = goal + 0
        if goal_angle:
            self.goal_angle = goal_angle
        self.move_state = Constant.MOVE_STATE_MOVE
        self.prev_goal_time = rospy.Time.now().to_sec()
        return True

    def setExploreRegion(self, region_xy_min, region_xy_max):
        if not self.grid_map.initialized:
            return False
        self.grid_map.setExploreRegion(region_xy_min, region_xy_max)
        return True

    def cancel_goal(self):
        self.client.cancel_goal()
        self.move_state = Constant.MOVE_STATE_IDLE
        # print("robot: cancel the goal")

    def pause_robot(self):
        if self.move_state == Constant.MOVE_STATE_MOVE:
            self.move_state = Constant.MOVE_STATE_PAUSE
            self.client.cancel_goal()
            # print("robot: pause")

    def continue_robot(self):
        if self.move_state == Constant.MOVE_STATE_PAUSE:
            self.move_state = Constant.MOVE_STATE_MOVE
            # print("robot: continue")

    def nextGoal(self):
        next_xy = np.zeros(2)
        if not self.grid_map.region_set:
            return False, next_xy
        remain_x, remain_y = self.grid_map.remainExploreGoals()
        if len(remain_x) == 0:
            return False, next_xy
        curr_xy = self.curr_pose[:2]
        curr_xy = curr_xy.reshape(1,-1)
        remain_xy = np.stack((remain_x, remain_y),axis=1)
        next_index = cdist(curr_xy, remain_xy).argmin(axis=1)[0]
        next_xy = remain_xy[next_index]
        return True, next_xy

    def visulizationCallback(self, msg=None):
        explored_x, explored_y = self.grid_map.exploredGoals(flag_index=True)
        new_msg = RobotVisualization()
        new_msg.robot_id = self.robot_id
        new_msg.explored_x = explored_x
        new_msg.explored_y = explored_y
        self.visulization_pub.publish(new_msg)

    def setPick(self, block_ids):
        self.pick_block_ids = [block_id for block_id in block_ids]
        return True

    def pick(self):
        # print('pick_block_id', self.pick_block_ids, self.pick_block_id, 'joint_angle_id', self.joint_angle_id, 'picked_num = ', self.picked_num)
        if self.pick_block_id is None or self.pick_block_id >= len(self.block_T) or self.block_T[self.pick_block_id] is None:
            self.joint_angle_id = -1
            self.flag_picking = False
            if self.pick_block_ids:
                self.pick_block_id = self.pick_block_ids.pop()
                # print('return 1a', self.flag_picking, self.pick_block_ids)
                return False
            else:
                self.cancel_goal()
                self.pick_block_id = None
                self.pick_block_ids = []
                # print('return 1b', self.flag_picking, self.pick_block_ids)
                return True # Finish the task
        block_in_robot = np.linalg.inv(self.curr_T).dot(self.block_T[self.pick_block_id])
        block_trans_in_robot = block_in_robot[:2, 3]
        # print('block_trans_in_robot = ', block_trans_in_robot)
        # Move to the block
        if not self.flag_picking:
            if self.flag_gtpose:
                flag_reached = block_trans_in_robot[0] > 0.0 and block_trans_in_robot[0] < 0.11 and np.abs(block_trans_in_robot[1] - 0.000) < 0.04
            else:
                flag_reached = block_trans_in_robot[0] > 0.0 and block_trans_in_robot[0] < 0.11 and np.abs(block_trans_in_robot[1] - 0.000) < 0.07
            if self.move_state == Constant.MOVE_STATE_IDLE:
                if not flag_reached:
                    next_goal = self.block_T[self.pick_block_id][:2, 3]
                    self.setGoal(next_goal)
            if flag_reached:
                self.cancel_goal()
                self.flag_picking = True
            elif rospy.Time.now().to_sec() - self.prev_goal_time > 5.0:
                # Need to replan
                print('Trigger cancel goal.')
                self.cancel_goal()
                self.flag_picking = False
                
        # Pick up the block
        if self.flag_picking:
            if self.flag_gtpose:
                flag_reached = block_trans_in_robot[0] > 0.0 and block_trans_in_robot[0] < 0.11 and np.abs(block_trans_in_robot[1] - 0.000) < 0.055
            else:
                flag_reached = block_trans_in_robot[0] > 0.0 and block_trans_in_robot[0] < 0.13 and np.abs(block_trans_in_robot[1] - 0.000) < 0.09
            if (not flag_reached) and self.joint_angle_id < self.joint_angle_attach_id:
                print('Trigger repick.')
                self.flag_picking = False
                self.attach(self.pick_block_id, flag_attach=False)

                self.joint_angle_id = -1
                joint_angles = [self.joint_angles[self.joint_angle_id]]
                gripper_states = [self.gripper_states[self.joint_angle_id]]
                total_time = self.pick_message_time_gap * 0.4
                self.arm.publish_commands(joint_angles, gripper_states, total_time)
                # print('return 4', self.flag_picking, self.pick_block_ids)
                return False

            if rospy.Time.now().to_sec() - self.prev_pick_message_time > self.pick_message_time_gap:
                if self.joint_angle_id == -1:
                    self.joint_angles[3] = self.detach_angles[self.picked_num % self.detach_num]
                    self.joint_angles[4] = self.detach_angles[self.picked_num % self.detach_num]
                self.prev_pick_message_time = rospy.Time.now().to_sec()
                self.joint_angle_id += 1

                if self.joint_angle_id >= len(self.joint_angles):
                    self.joint_angle_id = -1
                    self.flag_picking = False
                    self.picked_num += 1
                    if self.pick_block_ids:
                        self.pick_block_id = self.pick_block_ids.pop()
                        # print('return 2a', self.flag_picking, self.pick_block_ids)
                        return False
                    else:
                        self.cancel_goal()
                        # print('return 2b', self.flag_picking, self.pick_block_ids)
                        self.pick_block_id = None
                        self.pick_block_ids = []
                        return True # Finish the task
                if self.joint_angle_id == self.joint_angle_attach_id:
                    self.attach(self.pick_block_id, flag_attach=True)
                elif self.joint_angle_id == self.joint_angle_detach_id:
                    self.attach(self.pick_block_id, flag_attach=False)

                joint_angles = [self.joint_angles[self.joint_angle_id]]
                gripper_states = [self.gripper_states[self.joint_angle_id]]
                total_time = self.pick_message_time_gap * 0.8
                self.arm.publish_commands(joint_angles, gripper_states, total_time)
        # print('return 3', self.flag_picking, self.pick_block_ids)
        return False

    def explore(self):
        if rospy.Time.now().to_sec() - self.prev_goal_time > 5.0:
            # Need to replan
            print('Trigger replan.')
            self.cancel_goal()
        flag_next, next_goal = self.nextGoal()
        if not flag_next: # All goals have been explored
            self.cancel_goal()
            return True
        if self.move_state == Constant.MOVE_STATE_IDLE:
            self.setGoal(next_goal)
        return False
    
    def find(self):
        if rospy.Time.now().to_sec() - self.prev_goal_time > 5.0:
            # Need to replan
            # print('Trigger replan.')
            self.cancel_goal()
        # print('find_block_id', self.pick_block_ids, self.pick_block_id)
        # if len(self.pick_block_ids) <= 0:
        #     # This should not happen and return fake True
        #     self.cancel_goal()
        #     return Constant.ACTION_ID_SUCCESS
        if self.pick_block_id is None:
            if self.pick_block_ids:
                self.pick_block_id = self.pick_block_ids.pop()
        if self.pick_block_id >= len(self.block_T):
            # This should not happen and return fake True
            self.cancel_goal()
            self.pick_block_id = None
            return Constant.ACTION_ID_SUCCESS
        if self.block_T[self.pick_block_id] is not None:
            block_in_robot = np.linalg.inv(self.curr_T).dot(self.block_T[self.pick_block_id])
            block_trans_in_robot = block_in_robot[:2, 3]
            temp_dist = np.sqrt((block_trans_in_robot**2).sum())
            if temp_dist <= self.sensor_range:
                # Find the block
                self.cancel_goal()
                self.pick_block_id = None
                return Constant.ACTION_ID_FIND
        if self.explore():
            return Constant.ACTION_ID_SUCCESS
        return Constant.ACTION_ID_ONGOING

    def scanCallback(self, scan_msg):
        self.updatePose()
        if not (self.initialized and self.grid_map.initialized and self.grid_map.region_set):
            return
        d_angle = scan_msg.angle_increment
        beam_num = len(scan_msg.ranges)
        start_x_cell, start_y_cell = self.grid_map.meter2cell(self.curr_pose[0], self.curr_pose[1])
        start_cell = [start_x_cell, start_y_cell]
        for i_beam in range(beam_num):
            beam_angle = self.curr_pose[2] + scan_msg.angle_min + d_angle * i_beam
            beam_range = scan_msg.ranges[i_beam]
            # if beam_range > scan_msg.range_max or beam_range < scan_msg.range_min:
            if beam_range < scan_msg.range_min: # TODO: CHeck whether this should be <= and >=
                continue
            if beam_range > self.sensor_range: # Imagined sensor range
                beam_range = self.sensor_range
            end_x = self.curr_pose[0] + beam_range * math.cos(beam_angle)
            end_y = self.curr_pose[1] + beam_range * math.sin(beam_angle)
            end_x_cell, end_y_cell = self.grid_map.meter2cell(end_x, end_y)
            end_cell = [end_x_cell, end_y_cell]
            raycell_list = self.grid_map.cellAlongRay(start_cell, end_cell)
            self.grid_map.setAsExplored(raycell_list)

    def attach(self, block_id, flag_attach=True):
        # This function attaches or detaches the link when the block is in the robot's grippers
        req = AttachRequest()
        req.model_name_1 = "robot" + str(self.robot_id)
        req.link_name_1 = "link5" # gripper_link, end_effector_link, link5
        req.model_name_2 = "block" + str(block_id)
        req.link_name_2 = "base_footprint" # base_footprint, base_link
        if flag_attach:
            if not self.flag_attach[block_id]:
                self.attach_srv.call(req)
                self.flag_attach[block_id] = True
        else:
            if self.flag_attach[block_id]:
                self.detach_srv.call(req)
                self.flag_attach[block_id] = False
