from re import I
import numpy as np
from sympy import false
import rospy
from nav_msgs.msg import OccupancyGrid
import cv2 

class GridMap():
    def __init__(self, ros_map = None):
        '''This map will take ros Occupancy grid as input, initialize the internal map, and record all obstacle or unexplored region form Ocuupancy grid in to a np array as the internal map'''
        self.MAP_UNKNOWN = -1
        self.MAP_OBSTACLE = 50

        self.height = 1
        self.width = 1
        self.meter_per_cell = 1.0
        self.cell_per_meter = 1.0
        self.origin = np.array([0.0, 0.0])
        self.map             = np.array([])
        self.map_x_meter     = np.array([])
        self.map_y_meter     = np.array([])
        self.map_x_cell      = np.array([])
        self.map_y_cell      = np.array([])
        self.map_explorable  = np.array([])
        self.map_toexplore   = np.array([])
        self.map_explored    = np.array([])
        self.map_traversable = np.array([])
        self.map_totraverse  = np.array([])
        self.map_traversed   = np.array([])
        self.robot_radius = 6 # TODO: This is a hard code
        self.flag_erode = True
        self.x_cell_explored = []
        self.y_cell_explored = []
        self.x_meter_explored = []
        self.y_meter_explored = []
        self.map_appended = np.array([])

        self.initialized = False
        self.region_set = False
        rospy.Subscriber("map", OccupancyGrid, self.mapCallback)

    def updateMap(self, ros_map:OccupancyGrid):
        self.height = ros_map.info.height
        self.width = ros_map.info.width
        self.meter_per_cell = ros_map.info.resolution
        self.cell_per_meter = 1.0 / self.meter_per_cell

        self.origin = np.array([ros_map.info.origin.position.x, ros_map.info.origin.position.y])
        self.map = np.array(ros_map.data).reshape((self.height, self.width)) # self.map[y, x]
        self.map_explorable = np.logical_and(self.map != self.MAP_UNKNOWN, self.map < self.MAP_OBSTACLE) # Binary
        self.map_traversable = self.traversableMap(self.map_explorable)
        self.map_appended = np.zeros_like(self.map_explorable)

        temp_x_cell = np.arange(self.width)
        temp_y_cell = np.arange(self.height)
        temp_x_meter, temp_y_meter = self.cell2meter(temp_x_cell, temp_y_cell)
        self.map_x_meter, self.map_y_meter = np.meshgrid(temp_x_meter, temp_y_meter)
        self.map_x_cell , self.map_y_cell  = np.meshgrid(temp_x_cell, temp_y_cell)

        self.initialized = True

        # np.savetxt("map.csv", self.map, delimiter=",")
        # np.savetxt("map_traversable.csv", self.map_traversable, delimiter=",")

        print('self.map = ', self.map.min(), self.map.max())
        print('self.map.origin = ', self.origin)
        print('self.map.meter_per_cell = ', self.meter_per_cell)
        print('self.map.dtype = ', self.map.dtype)
        print('Map initialized')
        # temp_image = (self.map - self.map.min()) / (self.map.max() - self.map.min())
        # cv2.imshow('map show', temp_image)
        # cv2.waitKey(0)

    def mapCallback(self, ros_map:OccupancyGrid):
        # For now, only update once
        if not self.initialized:
            self.updateMap(ros_map)
    
    def meter2cell(self, x_meter, y_meter):
        if type(x_meter) == float:
            x_meter = np.array(x_meter, dtype=np.float64)
        if type(y_meter) == float:
            y_meter = np.array(y_meter, dtype=np.float64)
        x_cell = ((x_meter - self.origin[0]) * self.cell_per_meter).astype(np.int64)
        y_cell = ((y_meter - self.origin[1]) * self.cell_per_meter).astype(np.int64)
        x_cell_clip = x_cell.clip(0, self.width)
        y_cell_clip = y_cell.clip(0, self.height)
        return x_cell_clip, y_cell_clip

    def cell2meter(self, x_cell, y_cell):
        if type(x_cell) != np.int64:
            x_cell = np.array(x_cell, dtype=np.int64)
            y_cell = np.array(y_cell, dtype=np.int64)
        x_cell_clip = x_cell.clip(0, self.width)
        y_cell_clip = y_cell.clip(0, self.height)
        x_meter = (x_cell_clip * self.meter_per_cell + self.origin[0]).astype(np.float64)
        y_meter = (y_cell_clip * self.meter_per_cell + self.origin[1]).astype(np.float64)
        return x_meter, y_meter

    def traversableMap(self, map_explorable):
        # This is a placeholder method to get the obstacle distance map
        # Use a cv2.erode filter to get the map
        if self.flag_erode:
            kernel = np.ones((3,3), np.uint8)
            map_traversable = cv2.erode(map_explorable.astype(np.uint8) * 100, kernel, iterations=self.robot_radius).astype(bool)
        else:
            map_traversable = map_explorable.copy()
        # map_traversable = map_explorable.copy()
        # print('map_explorable', map_explorable.shape, map_explorable.dtype, map_explorable.any(), map_explorable.all())
        # print('map_traversable', map_traversable.shape, map_traversable.dtype, map_traversable.any(), map_traversable.all())
        # cv2.imwrite('./image1.png', map_explorable.astype(np.uint8)* 100)
        # cv2.imwrite('./image2.png', map_traversable.astype(np.uint8)* 100)

        return map_traversable

    def setExploreRegion(self, ori_region_xy_min, ori_region_xy_max):
        if not self.initialized:
            return
        if type(ori_region_xy_min) == list or type(ori_region_xy_min) == tuple:
            region_xy_min_list = np.array([ori_region_xy_min])
            region_xy_max_list = np.array([ori_region_xy_max])
        elif ori_region_xy_min.ndim == 1:
            region_xy_min_list = ori_region_xy_min.reshape(1, -1)
            region_xy_max_list = ori_region_xy_max.reshape(1, -1)
        else:
            region_xy_min_list = ori_region_xy_min
            region_xy_max_list = ori_region_xy_max
        region_mask = np.zeros(self.map.shape, dtype=bool)
        for i_region in range(region_xy_min_list.shape[0]):
            region_xy_min = region_xy_min_list[i_region]
            region_xy_max = region_xy_max_list[i_region]
            print('region_xy_min = ', i_region,  region_xy_min)
            print('region_xy_max = ', i_region, region_xy_max)
            cell_x1, cell_y1 = self.meter2cell(region_xy_min[0], region_xy_min[1])
            cell_x2, cell_y2 = self.meter2cell(region_xy_max[0], region_xy_max[1])
            region_mask[cell_y1:cell_y2, cell_x1:cell_x2] = True
        self.map_toexplore = np.logical_and(region_mask, self.map_explorable)
        self.map_totraverse = np.logical_and(region_mask, self.map_traversable)
        self.map_explored = np.zeros(self.map.shape, dtype=bool)
        self.map_traversed = np.zeros(self.map.shape, dtype=bool)
        self.region_set = True

        # temp_image = 
        # cv2.imshow('map show', temp_image)
        # cv2.waitKey(0)

    def remainExploreGoals(self):
        if not (self.initialized and self.region_set):
            return np.array([]), np.array([])
        map_remain = np.logical_and(np.logical_not(self.map_traversed), self.map_totraverse)
        remain_x = self.map_x_meter[map_remain]
        remain_y = self.map_y_meter[map_remain]
        return remain_x, remain_y

    def exploredGoals(self, flag_index):
        if not (self.initialized and self.region_set):
            return [], []
        # print('self.map_toexplore.shape', self.map_toexplore.shape, self.map_toexplore.dtype)
        # print('self.map_x_cell.shape', self.map_x_cell.shape, self.map_x_cell.dtype)
        # print('self.map_y_cell.shape', self.map_y_cell.shape, self.map_y_cell.dtype)
        # print('self.map_explored.shape', self.map_explored.shape, self.map_explored.dtype)
        # print('explored_x.shape', explored_x.shape, explored_x.dtype)
        if flag_index:
            explored_x = self.x_cell_explored
            explored_y = self.y_cell_explored
            # explored_x = self.map_x_cell[self.map_explored]
            # explored_y = self.map_y_cell[self.map_explored]
            # python3: malloc.c:2379: sysmalloc: Assertion `(old_top == initial_top (av) && old_size == 0) || ((unsigned long) (old_size) >= MINSIZE && prev_inuse (old_top) && ((unsigned long) old_end & (pagesize - 1)) == 0)' failed.
            # ind_row, ind_col = np.nonzero(self.map_explored)
            # explored_x = self.map_x_cell[ind_row, ind_col]
            # explored_y = self.map_y_cell[ind_row, ind_col]
        else:
            explored_x = self.x_meter_explored
            explored_y = self.y_meter_explored
            # explored_x = self.map_x_meter[self.map_explored]
            # explored_y = self.map_y_meter[self.map_explored]
        # return explored_x.tolist(), explored_y.tolist()
        return explored_x, explored_y

    def setAsExplored(self, cell_list):
        if not (self.initialized and self.region_set):
            return
        if len(cell_list) == 0:
            return
        temp_explored = np.zeros(self.map.shape, dtype=bool)
        temp_explored[cell_list[:,1], cell_list[:,0]] = True
        self.map_explored |= np.logical_and(self.map_toexplore, temp_explored)
        self.map_traversed = np.logical_and(self.map_explored, self.map_totraverse)
        # Update explore_x and explore_y
        for i_cell in range(cell_list.shape[0]):
            if self.map_toexplore[cell_list[i_cell,1], cell_list[i_cell,0]] and not self.map_appended[cell_list[i_cell,1], cell_list[i_cell,0]]:
                self.map_appended[cell_list[i_cell,1], cell_list[i_cell,0]] = True
                self.x_cell_explored.append(cell_list[i_cell,0].item())
                self.y_cell_explored.append(cell_list[i_cell,1].item())
                temp_x_meter, temp_y_meter = self.cell2meter(cell_list[i_cell,0], cell_list[i_cell,1])
                self.x_meter_explored.append(temp_x_meter.item())
                self.y_meter_explored.append(temp_y_meter.item())
    
    def forceExplored(self, cell_x, cell_y):
        if not (self.initialized and self.region_set):
            return
        if len(cell_x) == 0:
            return
        if type(cell_x[0]) == int:
            flag_int = True
        else:
            flag_int = False
        for i_cell in range(len(cell_x)):
            if not self.map_appended[cell_y[i_cell], cell_x[i_cell]]:
                self.map_appended[cell_y[i_cell], cell_x[i_cell]] = True
                if flag_int:
                    self.x_cell_explored.append(cell_x[i_cell])
                    self.y_cell_explored.append(cell_y[i_cell])
                else:
                    self.x_cell_explored.append(cell_x[i_cell].item())
                    self.y_cell_explored.append(cell_y[i_cell].item())
                temp_x_meter, temp_y_meter = self.cell2meter(cell_x[i_cell], cell_y[i_cell])
                self.x_meter_explored.append(temp_x_meter.item())
                self.y_meter_explored.append(temp_y_meter.item())

    def cellAlongRay(self, start, end):
        raycell_list = []
        dx = abs(end[0]-start[0])
        dy = abs(end[1]-start[1])
        sx = 0
        if(start[0] < end[0]):
            sx = 1 
        else:
            sx = -1
        sy = 0
        if(start[1] < end[1]):
            sy = 1 
        else:
            sy = -1
        error = dx - dy
        raycell = np.array([start[0],start[1]])
        while (raycell[0] != end[0] or raycell[1] != end[1]):
            # if self.initialized and (not self.map_explorable[raycell[1], raycell[0]]):
            #     break
            raycell_list.append(raycell + 0)
            e2 = 2 * error
            if (e2 >= -dy):
                error -= dy
                raycell[0] += sx
            if (e2 <= dx):
                error += dx
                raycell[1] += sy
        raycell_list = np.array(raycell_list)
        return raycell_list

    def traversable(self, goal):
        x_cell, y_cell = self.meter2cell(goal[0], goal[1])
        # print('great goal = ', goal)
        # print('great cell = ', x_cell, y_cell)
        # print('great map, map_traversable = ', self.map[y_cell, x_cell], self.map_traversable[y_cell, x_cell])
        return self.map_traversable[y_cell, x_cell]
'''
nav_msgs.msg: OccupancyGrid 
header: 
  seq: 0
  stamp: 
    secs: 1644559458
    nsecs: 993737883
  frame_id: "/map"
info: 
  map_load_time: 
    secs: 1644559458
    nsecs: 993737496
  resolution: 0.05000000074505806
  width: 384
  height: 384
  origin: 
    position: 
      x: -10.0
      y: -10.0
      z: 0.0
    orientation: 
      x: 0.0
      y: 0.0
      z: 0.0
      w: 1.0
data: []
'''