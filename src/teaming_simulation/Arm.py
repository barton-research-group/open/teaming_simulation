import rospy
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from sensor_msgs.msg import JointState
import numpy as np

class Arm:
    def __init__(self, robot_id):
        self.joint_name = ["joint1","joint2","joint3","joint4"]
        self.gripper_name = ["gripper", "gripper_sub"]
        # self.joint_name = ["joint1","joint2","joint3","joint4","gripper"]
        self.joint_pub = rospy.Publisher('robot' + str(robot_id) + '/arm_controller/command', JointTrajectory, queue_size=10)
        self.gripper_pub = rospy.Publisher('robot' + str(robot_id) + '/gripper_controller/command', JointTrajectory, queue_size=10)
        rospy.Subscriber("robot" + str(robot_id) + "/joint_states", JointState, self.joint_callback)
        self.curr_velocity = np.zeros(6)
        self.curr_position = np.zeros(6)

    def publish_commands(self, joint_angles, gripper_states, total_time):
        angle_num = len(joint_angles)
        if angle_num > 0:
            dt = total_time / angle_num
            new_joint_traj = JointTrajectory()
            new_joint_traj.header.stamp = rospy.Time.now()
            new_joint_traj.joint_names = self.joint_name
            for index in range(0, angle_num):
                angle = joint_angles[index]
                new_joint = JointTrajectoryPoint()
                new_joint.positions = angle
                # new_joint.effort = []
                new_joint.time_from_start =  rospy.Duration.from_sec(dt* (index+1) )
                new_joint_traj.points.append(new_joint)
            self.joint_pub.publish(new_joint_traj)
            print('joint', new_joint_traj)

        angle_num = len(gripper_states)
        if angle_num > 0:
            dt = total_time / angle_num
            new_joint_traj = JointTrajectory()
            new_joint_traj.joint_names = self.gripper_name
            new_joint_traj.header.stamp = rospy.Time.now()
            for index in range(angle_num):
                angle = gripper_states[index]
                new_joint = JointTrajectoryPoint()
                new_joint.positions = angle
                # new_joint.velocities = []
                # new_joint.effort = angle
                new_joint.time_from_start =  rospy.Duration.from_sec(dt* (index+1) )
                new_joint_traj.points.append(new_joint)
            self.gripper_pub.publish(new_joint_traj)
            print('gripper', new_joint_traj)

        rospy.loginfo("new position for arm successfully published")

    def joint_callback(self, msg):
        self.curr_velocity = np.array(msg.velocity)
        self.curr_position = np.array(msg.position)
        # print('self.curr_position', self.curr_position)
    
    def joint_diff(self, joint):
        return np.abs(self.curr_position[:4] - joint[:4]).max()
