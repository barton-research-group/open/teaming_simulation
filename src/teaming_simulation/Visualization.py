from visualization_msgs.msg import Marker, MarkerArray
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import PointStamped, Quaternion
import inspect

def PrintFrame():
    callerframerecord = inspect.stack()[1]    # 0 represents this line
                                            # 1 represents line at caller
    frame = callerframerecord[0]
    info = inspect.getframeinfo(frame)
    print(info.filename, ', ', info.function, ', ', info.lineno)                      # __FILE__     -> Test.py

def getSquareMarker(point_x, point_y, d_x, d_y):
    marker_array = MarkerArray()
    for i_square in range(point_x.shape[0]):
        marker = Marker()
        marker.header.frame_id = "map"
        marker.id = i_square
        marker.type = Marker.CUBE
        marker.action = Marker.ADD
        marker.pose.position.x = point_x[i_square] + 0.5 * d_x[i_square]
        marker.pose.position.y = point_y[i_square] + 0.5 * d_y[i_square]
        q = quaternion_from_euler(0, 0, 0)
        marker.pose.orientation = Quaternion(*q)
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 1.0
        marker.color.a = 0.1
        marker.scale.x = d_x[i_square]
        marker.scale.y = d_y[i_square]
        marker.scale.z = 0.1
        marker.frame_locked = False
        marker.ns = str(i_square) + "_region"
        marker_array.markers.append(marker)
    return marker_array

def getExploredMarker(point_x, point_y):
    marker_size = 0.1
    marker_array = MarkerArray()
    for i_marker in range(len(point_x)):
        marker = Marker()
        marker.header.frame_id = "map"
        marker.id = i_marker
        marker.type = Marker.SPHERE
        marker.action = Marker.ADD
        marker.pose.position.x = point_x[i_marker] + 0.5*marker_size
        marker.pose.position.y = point_y[i_marker] + 0.5*marker_size
        q = quaternion_from_euler(0, 0, 0)
        marker.pose.orientation = Quaternion(*q)
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 1.0
        marker.color.a = 0.5
        marker.scale.x = marker_size
        marker.scale.y = marker_size
        marker.scale.z = marker_size
        marker.frame_locked = False
        marker.ns = str(i_marker) + "__explored"
        marker_array.markers.append(marker)
    return marker_array
