# Teaming Simulation
A multi-agent simulation where multiple heterogeneous robots copperatively comduct multiple heterogeneous exploration and block-picking style tasks.

This [video](https://youtu.be/r_7ZsEk6axU&t=304) shows one of the senarios where six robots collaboratively complete five heterogeneous tasks distributed in the task area.

This simulation supports the following projects. 
- Robust task scheduling for heterogeneous robot teams under capability uncertainty [(repository)](https://gitlab.com/barton-research-group/open/resilient_team_planner) [1] [2]
- Learning task requirements and agent capabilities for multi-agent task allocation [(repository)](https://gitlab.com/barton-research-group/open/learn-multiagent-taskreq) [3]

# Installation Step

## Step1: [Install ROS Noetic](https://wiki.ros.org/noetic/Installation/Ubuntu)

## Step2: Install other packages

``` bash
# Install turtlebot related
sudo apt-get install ros-noetic-turtlebot3-gazebo-dbgsym
sudo apt-get install ros-noetic-turtlebot3-fake-dbgsym
sudo apt-get install ros-noetic-turtlebot3-gazebo
sudo apt-get install ros-noetic-turtlebot3-example
sudo apt-get install ros-noetic-turtlebot3-fake
sudo apt-get install ros-noetic-turtlebot3-slam-dbgsym
sudo apt-get install ros-noetic-turtlebot3-description
sudo apt-get install ros-noetic-turtlebot3-bringup-dbgsym
sudo apt-get install ros-noetic-turtlebot3-msgs
sudo apt-get install ros-noetic-turtlebot3-bringup
sudo apt-get install ros-noetic-turtlebot3-navigation
sudo apt-get install ros-noetic-turtlebot3-simulations
sudo apt-get install ros-noetic-turtlebot3-slam
sudo apt-get install ros-noetic-turtlebot3-teleop
# Install plannar
sudo apt-get install ros-noetic-dwa-local-planner
# Install SLAM
sudo apt-get install ros-noetic-slam-gmapping
# Install moveit
sudo apt-get install ros-noetic-moveit-ros
```

## Step3: Initialize the ROS Workspace

### Install from Gitlab

```bash
cd ~
mkdir teaming_ws
cd teaming_ws
mkdir src
cd src
git clone git@gitlab.com:barton-research-group/open/teaming_simulation.git
git clone git@gitlab.com:barton-research-group/resilient-teaming/gridmapping.git
git clone git@gitlab.com:barton-research-group/resilient-teaming/turtlebot3_manipulation.git # forked from https://github.com/ROBOTIS-GIT/turtlebot3_manipulation.git
git clone git@gitlab.com:barton-research-group/resilient-teaming/turtlebot3_manipulation_simulations.git # forked from https://github.com/ROBOTIS-GIT/turtlebot3_manipulation_simulations.git
git clone https://github.com/ROBOTIS-GIT/open_manipulator_dependencies.git
git clone https://github.com/pal-robotics/gazebo_ros_link_attacher.git
```

### Install from files

Extract the package from the .zip or .tar file. First run the following commands.

```bash
cd ~
mkdir teaming_ws
cd teaming_ws
mkdir src
cd src
```

Then, copy the following repositories to `~/teaming_ws/src/`:
- teaming_simulation
- gridmapping
- turtlebot3_manipulation
- turtlebot3_manipulation_simulations
- open_manipulator_dependencies
- gazebo_ros_link_attacher

## Step4: Build ROS workspace

``` bash
cd ~/teaming_ws
catkin build
source devel/setup.bash
```

If catkin build fails due to wrong version of sdformat: 'turtlebot3_gazebo' specifies '/usr/include/sdformat-9.5', [download](http://sdformat.org/download) and [install](http://sdformat.org/tutorials?tut=install) the correct version.

It might also work to manually modify the `turtlebot3_gazebo.cmake` file to specify the installed version.

## Step5： Run an example scenario

Run the following to run a cooperative task in simulation.

```bash
roslaunch teaming_simulation multibot.launch # open the Gazebo environment; call the robot control, localization, and path planning packages
# In another terminal window
rosrun teaming_simulation task_sim.py # by defauly, a pick and place task will start
```

To change the tasks of the the robots, change the `flag_task` in task_sim.py. By default, a cooperative pick and place task will be shown.

# Run the multi-agent simulation

Set the `flag_task` in `script/loop_case.py.py` and `script/task_sim.py`.
- 1:        cooperative exploration
- 2, 3, 4:  pick and place with different setups
- 5:        explore a region to find a block and then pick it up
- 10:       multi-agent task allocation including all the tasks from 1-5

Set the `flag_read_yaml` in `script/task_sim.py` to `True`.

Choose an `i_case` such that `i_case < len(robot_type_array_list)`.

First generate a temporary test case (`temp_case.launch` and `temp_case.yaml` in `launch/`) by
```bash
cd script
python3 loop_case.py   # Generate a test case
```

Then launch the generated test case and the task allocator.
```bash
roslaunch teaming_simulation temp_case.launch   # Launch the generated test case
# In another terminal window
rosrun teaming_simulation task_sim.py           # Launch the task allocator
```

NOTE: If you want to run `flag_task == 10`, before launch the gazebo, move to the main folder of this repository (`teaming_simulation`) and run 
```bash
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$(pwd)/models/
```

A screenshot of the expected view is shown below.

<img src="readme/square_world.png" alt="square_world" width="100%"/>

# Run test cases that call a partial functionality of the simulator
## Case 1: Arm movement

Run the following small example to see how to control the arm movement of one of the robot.

```bash
roslaunch teaming_simulation multibot.launch
rosrun teaming_simulation armbot_test.py # An example python file to control the arm
```

## Case 2: Robot actions

Run the following small example to see a robot picking up a block.

```bash
roslaunch teaming_simulation multibot.launch
rosrun teaming_simulation robot_action_test.py # An example python file to call a robot with an initial action
```

To publish additional actions besides the default action set in robot_action_test.py, change the action parameters in action_publisher.py and run the following command.

The default action published by action_publisher moves the robot to a specific point in the map.

```bash
rosrun teaming_simulation action_publisher.py # An example python file to publish an action to a robot
```
## Case 3: Genearte a new map

Change the `worlds/square.world` in `launch/world.launch` to other world files to map new world. For a new world file, we need the a file saved using gazebo. E.g., `world_name.world`.

Open the simulator and the SLAM algorithm.

``` bash
export TURTLEBOT3_MODEL=burger
roslaunch teaming_simulation world.launch 
```

In another command window, drive the robot around using the keyboard to Create the Map.
```bash
export TURTLEBOT3_MODEL=burger
roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch  
```

Save Map  
```
rosrun map_server map_saver -f ~/map
```

The new map files (.pgm and .yaml) wil be saved in `~/`. You can manually move them to `worlds/`.

Also, for a new .world file, add the following line 
```xml
<plugin name="ros_link_attacher_plugin" filename="libgazebo_ros_link_attacher.so"/>
```
right after
```xml
<?xml version="1.0" ?>
<sdf version='1.7'>
  <world name='default'>
```

## Case 4: Fixed multi-agent task scenarios

Set `flag_read_yaml` in `script/task_sim.py` to `False`.

Set the `flag_task` in `script/task_sim.py` to
- 20: two agents explore three regions
- 21: two agents first explora a region and then pick up the blocks in the room.
- 22: a robot moves to the goal location

```bash
roslaunch teaming_simulation multibot.launch # open the Gazebo environment; call the robot control, localization, and path planning packages
# In another terminal window
rosrun teaming_simulation task_sim.py # by defauly, a pick and place task will start
```

# Launch file structure
Launch file is structured as follow:
## Individual robot file

Each robot has its own individual launch file, consist of robot state publisher, spawn in to to gazebo, and navigation component. See the following launch files.

- one_robot.launch
- one_robot_witharm.launch

## Multiple Robot

The above single robot file is included in multi-robot launch files.

- multibot.launch


# Remark

Currently, the friction forces are not simulated in this simulator. When robot tries to pick up a block and the block is in its gripper, the link between the block and the gripper will be attached.

# Attributions
https://github.com/ROBOTIS-GIT/turtlebot3_manipulation.git

https://github.com/ROBOTIS-GIT/turtlebot3_manipulation_simulations.git

https://github.com/ROBOTIS-GIT/open_manipulator_dependencies.git

https://github.com/pal-robotics/gazebo_ros_link_attacher.git

# References

[1] B. Fu, W. Smith, D. Rizzo, M. Castanier, and K. Barton, “Heterogeneous vehicle routing and teaming with Gaussian distributed energy uncertainty,” in 2020 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS). IEEE, 2020, pp. 4315-4322 \[[PDF](https://arxiv.org/pdf/2010.11376.pdf)\]

[2] B. Fu, W. Smith, D. Rizzo, M. Castanier, M. Ghaffari, and K. Barton, “Robust task scheduling for heterogeneous robot teams under capability uncertainty,” arXiv preprint arXiv:2106.12111, 2021. \[[PDF](https://arxiv.org/pdf/2106.12111.pdf)\] \[[Video](https://youtu.be/gQR1CQV5z_0)\]

[3] B. Fu, W. Smith, D. Rizzo, M. Castanier, M. Ghaffari, and K. Barton, "Learning Task Requirements and Agent Capabilities for Multi-agent Task Allocation," arXiv preprint arXiv:2211.03286, 2022. \[[PDF](https://arxiv.org/pdf/2211.03286.pdf)\] \[[Video](https://youtu.be/r_7ZsEk6axU)\]