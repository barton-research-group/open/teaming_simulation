import sys
import rospy
from teaming_simulation.GazeboPosePublisher import GazeboPosePublisher

def main():
    rospy.init_node('slam_pose_publisher', anonymous=False)
    print(rospy.myargv())

    ros_args = rospy.myargv()

    gazebo_pub_list = []
    robot_link = 'turtlebot3_burger'
    robot_message = 'turtlebot3_burger'
    gazebo_pub = GazeboPosePublisher(robot_link, robot_message, publish_time = 0.03, flag_verbose=False)
    gazebo_pub_list.append(gazebo_pub)
    rospy.spin()

if __name__ == '__main__':
    main()
