from teaming_simulation.CaseIO import CaseIO

launch_file = '/home/bofu/Workspaces/sim_ws/src/teaming_simulation/launch/temp_case.launch'
yaml_file = '/home/bofu/Workspaces/sim_ws/src/teaming_simulation/launch/temp_case.yaml'

'''
    ROB_TYPE_SMALL = 0
    ROB_TYPE_SMALL_BROAD = 1
    ROB_TYPE_LARGE_NOARM = 2
    ROB_TYPE_LARGE = 3
    ROB_TYPE_LARGE_HEAVY = 4
    BLOCK_LIGHT = 0
    BLOCK_HEAVY = 1
'''

robot_type_array = [1, 1, 0, 1, 1]
block_type_array = [2, 2]

# pre_robot_x_list = [-1.5, -0.5, -2.5,  1.5]
# pre_robot_y_list = [-0.5, -1.5,  1.5, -2.5]

pre_robot_x_list = [-2.5,  -1.5, -0.5,  1.5]
pre_robot_y_list = [ 1.5,  -0.5, -1.5, -2.5]

# pre_block_x_list = [-0.95,  1.45,  0.50,  3.60]
# pre_block_y_list = [ 1.50, -1.45,  3.72,  1.53]
# pre_block_x_list = [-0.77,  3.95,  2.50,  3.50]
# pre_block_y_list = [ 2.91, -1.15,  4.15,  1.90]

pre_block_x_list = [-0.77,  2.50,  3.50,  3.95]
pre_block_y_list = [ 2.91,  4.15,  1.90, -1.15]

# Genearte the robot param
robot_type_list = []
for i_robot_type in range(5):
    for i_add in range(robot_type_array[i_robot_type]):
        robot_type_list.append(i_robot_type)
robot_num = len(robot_type_list)
robot_x_list = pre_robot_x_list[:robot_num]
robot_y_list = pre_robot_y_list[:robot_num]

# Genearte the block param
block_type_list = []
for i_block_type in range(2):
    for i_add in range(block_type_array[i_block_type]):
        block_type_list.append(i_block_type)
block_num = len(block_type_list)
block_x_list = pre_block_x_list[:block_num]
block_y_list = pre_block_y_list[:block_num]

case_io = CaseIO()
# Change the test case parameters
case_io.robot_type_list = robot_type_list 
case_io.robot_x_list = robot_x_list
case_io.robot_y_list = robot_y_list

case_io.block_type_list = block_type_list 
case_io.block_x_list = block_x_list
case_io.block_y_list = block_y_list


# Write the files
case_io.writeLaunch(launch_file)
case_io.writeYAML(yaml_file)
# case_io.readYAML(yaml_file)
