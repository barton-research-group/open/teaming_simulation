import rospy
import numpy as np 
from teaming_simulation.msg import RobotAction
from teaming_simulation.Constant import Constant

class ActionPublisher():
    def __init__(self):
        robot_id = 2
        flag_command = Constant.ACTION_STATE_MOVE

        self.action_msg = RobotAction()
        self.action_msg.robot_id = robot_id
        self.action_msg.action_type = flag_command
        self.action_msg.action_id = 12
        self.action_msg.block_ids = [0]
        self.action_msg.goal_pose = [-1.5, 0.5]
        # self.action_msg.goal_region = [0.68345731,  0.61214435,  1.59769261,  1.54366839]
        # self.action_msg.goal_region = [0.5236879,   -1.588027,    1.62432063, -0.63875961]
        # self.action_msg.goal_region = [-1.54443586, -0.32825109, -0.56806839,  0.55004281]
        # For find and pick
        # self.action_msg.goal_region = [-0.71, -1.49, 1.55, 1.72] # xmin, ymin, xmax, ymax
        # self.action_msg.goal_region = [-0.71, -1.49, 1.55, 0.5] # xmin, ymin, xmax, ymax

        self.topic_name = "/robot" + str(robot_id) + "/action"
        self.action_pub = rospy.Publisher(self.topic_name, RobotAction, queue_size=10)

        action_rate = rospy.Duration(3.0)
        rospy.Timer(action_rate, self.publishAction)
    
    def publishAction(self, msg=None):
        print("topic_name = ", self.topic_name)
        print("action_msg = ", self.action_msg)
        self.action_pub.publish(self.action_msg)


def main():
    rospy.init_node('action_publisher', anonymous=True)

    action_publisher = ActionPublisher()

    rospy.spin()

if __name__ == '__main__':
    main()



