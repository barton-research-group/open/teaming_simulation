import rospy
from teaming_simulation.GazeboPosePublisher import GazeboPosePublisher

def main():
    rospy.init_node('gazebo_pose_publisher', anonymous=False)
    print(rospy.myargv())

    ros_args = rospy.myargv()
    if len(ros_args) >= 2:
        block_num = int(ros_args[1])
    else:
        block_num = 0

    if len(ros_args) >= 3:
        block_prefix = ros_args[2]
    else:
        block_prefix = 'block'

    if len(ros_args) >= 4:
        i_offset = int(ros_args[3])
    else:
        i_offset = 0

    if len(ros_args) >= 5:
        publish_time = float(ros_args[4])
    else:
        publish_time = 0.03

    flag_publish_odom = False
    if block_prefix == 'robot':
        flag_publish_odom = True

    gazebo_pub_list = []
    for i_block in range(block_num):
        robot_link = block_prefix + str(i_block + i_offset)
        robot_message = '/' + block_prefix + str(i_block + i_offset) + '/base_footprint'
        robot_odom_message = '/' + block_prefix + str(i_block + i_offset) + '/odom'
        gazebo_pub = GazeboPosePublisher(robot_link, robot_message, publish_time = publish_time, flag_verbose=False, flag_publish_odom=flag_publish_odom, robot_odom_message=robot_odom_message)
        gazebo_pub_list.append(gazebo_pub)
    rospy.spin()

if __name__ == '__main__':
    main()
