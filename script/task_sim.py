import rospy
from teaming_simulation.TaskAllocator import TaskAllocator
from teaming_simulation.Robot import Robot
from teaming_simulation.PriorityMonitor import PriorityMonitor
from teaming_simulation.Constant import Constant
from teaming_simulation.CaseIO import CaseIO

def main():
    enable_visulization = True
    flag_read_yaml = False
    flag_task = 2
    # For the following tasks, set flag_read_yaml to true
    # flag_task
    # 1:        cooperative exploration
    # 2, 3, 4:  pick and place with different setups
    # 5:        explore a region to find a block and then pick it up
    # 10:       multi-agent task allocation including all the tasks from 1-5

    # The following hardcoded simple tests that work with 'multibot.launch', set flag_read_yaml to false
    # flag_task
    # 20:       two agents explore three regions
    # 21:       two agents first explora a region and then pick up the blocks in the room.
    # 22:       a robot moves to the goal location
    rospy.init_node('task_sim', anonymous=True)

    result_file_name = './temp_result.yaml'
    if flag_read_yaml:
        yaml_file = '/home/bofu/Workspaces/sim_ws/src/teaming_simulation/launch/temp_case.yaml'
        case_io = CaseIO()
        yaml_dict = case_io.readYAML(yaml_file, flag_return=True)
        robot_type_list = case_io.robot_type_list
        block_type_list = case_io.block_type_list
        if case_io.result_yaml:
            result_file_name = case_io.result_yaml
    else:
        robot_type_list = [Constant.ROB_TYPE_LARGE_HEAVY, Constant.ROB_TYPE_LARGE]
        block_type_list = [Constant.BLOCK_LIGHT, Constant.BLOCK_HEAVY, Constant.BLOCK_LIGHT]
        yaml_dict = None
    robot_num = len(robot_type_list)
    block_num = len(block_type_list)

    robot_list = []
    priority_list = []
    for i_robot in range(robot_num):
        robot_index = i_robot+1
        robot_type = robot_type_list[i_robot]
        priority = robot_num - i_robot
        # priority = i_robot
        robot = Robot(priority, block_num, robot_index, robot_type, enable_visulization)
        robot_list.append(robot)
        priority_list.append(priority)

    task_allocator = TaskAllocator(enable_visulization, robot_num, robot_type_list, block_type_list, result_file_name)
    task_allocator.setTask(flag_task=flag_task)
    if yaml_dict:
        task_allocator.setSetupDict(yaml_dict)
    priority_monitor = PriorityMonitor(task_allocator.int2id, task_allocator.id2int, priority_list)

    rospy.spin()

if __name__ == '__main__':
    main()

