from teaming_simulation.CaseIO import CaseIO

'''
    ROB_TYPE_SMALL = 0
    ROB_TYPE_SMALL_BROAD = 1
    ROB_TYPE_LARGE_NOARM = 2
    ROB_TYPE_LARGE = 3
    ROB_TYPE_LARGE_HEAVY = 4
    BLOCK_LIGHT = 0
    BLOCK_HEAVY = 1
'''

# flag_task
# 1:        cooperative exploration
# 2, 3, 4:  pick and place with different setups
# 5:        explore a region to find a block and then pick it up
# 10:       multi-agent task allocation including all the tasks from 1-5

flag_task = 2   # Control the task type
i_case = 0     # Control the robot number, i_case should be smaller than len(robot_type_array_list)
i_repeat = 0


launch_file = '../launch/temp_case.launch'
yaml_file = '../launch/temp_case.yaml'

backup_folder = '/home/bofu/Desktop/learn_case/learn_case3/'
backup_launch_folder = 'launch/'
backup_case_folder = 'case/'
backup_result_folder = 'result/'

flag_backup = False

if flag_task == 1:
    world_name = 'square'
    task_folder = 'explore/'
    block_type_array = [0, 0]
    pre_block_x_list = [-0.77,  2.10,  3.50,  3.95]
    pre_block_y_list = [ 2.91,  4.15,  1.90, -1.15]
    pre_robot_x_list = [-2.5,  -1.5, -0.5,  1.5]
    pre_robot_y_list = [ 1.5,  -0.5, -1.5, -2.5]
    robot_type_array_list = [[1, 0, 0, 0, 0],
                        [0, 1, 0, 0, 0],
                        [0, 0, 1, 0, 0],
                        [0, 0, 0, 1, 0],
                        [0, 0, 0, 0, 1],
                        [2, 0, 0, 0, 0],
                        [0, 2, 0, 0, 0],
                        [0, 0, 2, 0, 0],
                        [0, 0, 0, 2, 0],
                        [0, 0, 0, 0, 2],
                        [1, 1, 0, 0, 0],
                        [1, 0, 1, 0, 0],
                        [1, 0, 0, 1, 0],
                        [1, 0, 0, 0, 1],
                        [0, 1, 1, 0, 0],
                        [0, 1, 0, 1, 0],
                        [0, 1, 0, 0, 1],
                        [0, 0, 1, 1, 0],
                        [0, 0, 1, 0, 1],
                        [0, 0, 0, 1, 1]]
elif flag_task == 2:
    world_name = 'square'
    task_folder = 'picklight/'
    block_type_array = [4, 0]
    pre_block_x_list = [-0.77,  2.10,  3.50,  3.95]
    pre_block_y_list = [ 2.91,  4.15,  1.90, -1.15]
    pre_robot_x_list = [-2.5,  -1.5, -0.5,  1.5]
    pre_robot_y_list = [ 1.5,  -0.5, -1.5, -2.5]
    robot_type_array_list = [[0, 0, 0, 1, 0],
                        [0, 0, 0, 0, 1],
                        [0, 0, 0, 2, 0],
                        [0, 0, 0, 0, 2],
                        [0, 0, 0, 1, 1],
                        [0, 0, 0, 3, 0],
                        [0, 0, 0, 0, 3],
                        [0, 0, 0, 2, 1],
                        [0, 0, 0, 1, 2],
                        [0, 0, 0, 4, 0],
                        [0, 0, 0, 0, 4],
                        [0, 0, 0, 3, 1],
                        [0, 0, 0, 1, 3],
                        [0, 0, 0, 2, 2]] # 14
elif flag_task == 3:
    world_name = 'square'
    task_folder = 'pickboth/'
    block_type_array = [2, 2]
    pre_block_x_list = [1.58, -1.22,  3.50,  3.95]
    pre_block_y_list = [3.41,  4.13,  1.90, -1.15]
    pre_robot_x_list = [-2.5,  -1.5, -0.5,  1.5]
    pre_robot_y_list = [ 1.5,  -0.5, -1.5, -2.5]
    robot_type_array_list = [[0, 0, 0, 0, 1],
                        [0, 0, 0, 1, 1],
                        [0, 0, 0, 2, 1],
                        [0, 0, 0, 3, 1],
                        [0, 0, 0, 0, 2],
                        [0, 0, 0, 1, 2],
                        [0, 0, 0, 2, 2],
                        [0, 0, 0, 0, 3],
                        [0, 0, 0, 1, 3],
                        [0, 0, 0, 0, 4]] # 10
elif flag_task == 4:
    world_name = 'square'
    task_folder = 'pickheavy/'
    block_type_array = [0, 4]
    pre_block_x_list = [-0.77,  2.10,  3.90,  3.95]
    pre_block_y_list = [ 2.91,  4.15,  2.47, -1.15]
    pre_robot_x_list = [-2.5,  -1.5, -0.5,  1.5]
    pre_robot_y_list = [ 1.5,  -0.5, -1.5, -2.5]
    robot_type_array_list = [[0, 0, 0, 0, 1],
                        [0, 0, 0, 0, 2],
                        [0, 0, 0, 0, 3],
                        [0, 0, 0, 0, 4]] # 4
elif flag_task == 5:
    world_name = 'square'
    task_folder = 'find/'
    block_type_array = [1, 0]
    pre_block_x_list = [1.08,  1.73,  3.67,  2.97,  1.32]
    pre_block_y_list = [2.36,  3.58,  2.12,  0.45, -0.25]
    pre_robot_x_list = [-2.5,  -1.5, -0.5,  1.5]
    pre_robot_y_list = [ 1.5,  -0.5, -1.5, -2.5]
    robot_type_array_list = [[0, 0, 0, 1, 0],
                        [0, 0, 0, 0, 1],
                        [0, 0, 0, 2, 0],
                        [0, 0, 0, 0, 2],
                        [1, 0, 0, 1, 0],
                        [1, 0, 0, 0, 1],
                        [0, 1, 0, 1, 0],
                        [0, 1, 0, 0, 1],
                        [0, 0, 1, 1, 0],
                        [0, 0, 1, 0, 1],
                        [0, 0, 0, 1, 1],
                        [0, 0, 0, 3, 0],
                        [0, 0, 0, 0, 3],
                        [1, 0, 0, 1, 1],
                        [0, 1, 0, 1, 1],
                        [0, 0, 1, 1, 1],
                        [0, 1, 1, 0, 1],
                        [0, 1, 1, 1, 0],
                        [1, 1, 0, 0, 1],
                        [1, 1, 0, 1, 0],
                        [1, 1, 1, 1, 0],
                        [0, 1, 1, 1, 1],
                        [1, 0, 1, 1, 1],
                        [1, 1, 0, 1, 1],
                        [1, 1, 1, 0, 1]] # 25
elif flag_task == 10:
    world_name = 'heightmap30'
    task_folder = 'heightmap/'
    block_type_array = [7, 6]
    # pre_block_x_list = [9.83,      3.53,  5.75,  6.00,  8.75,      -7.26, -10.24,   -7.31, -9.74,      -4.58,  -7.59,  -6.87,   -10.02        ]
    # pre_block_y_list = [-2.68,     8.74,  7.40,  10.19,  8.73,     10.17,   9.28,    7.30,  6.72,       -4.41,  -2.94,   -6.98,    -4.96         ]
    pre_block_x_list = [10.83,   3.69,   4.67,  6.00,  8.75,      -7.26, -10.77,   -9.08, -9.74,            -10.02, -7.59, -4.58, -9.56        ]
    pre_block_y_list = [-2.68,    8.74, 7.34, 10.19,  8.73,     10.17,   8.63,    10.17,  6.72,       -4.96,     -2.94,    -4.41,  -0.48         ]
    pre_robot_x_list = [ 1.20,  1.20,  1.20, -0.00, -0.00, -0.00]
    pre_robot_y_list = [ 3.30,  0.30, -2.70,  3.30,  0.30, -2.70]
    robot_type_array_list = [[0, 1, 0, 2, 3]]
else: # flag_task == 11
    world_name = 'square'
    task_folder = 'heightmap/'
    block_type_array = [7, 6]
    # pre_block_x_list = [9.83,      3.53,  5.75,  6.00,  8.75,      -7.26, -10.24,   -7.31, -9.74,      -4.58,  -7.59,  -6.87,   -10.02        ]
    # pre_block_y_list = [-2.68,     8.74,  7.40,  10.19,  8.73,     10.17,   9.28,    7.30,  6.72,       -4.41,  -2.94,   -6.98,    -4.96         ]
    pre_block_x_list = [4.0,      -1.0, -1.0, 1.0, 1.0,         -3.0, -1.0, -3.0, -3.0,      1.0,  1.0,  1.0,  4.0       ]
    pre_block_y_list = [-2.0,     -3.0, -1.0, -1.0, -3.0,        4.0,  3.0,  1.0,  3.0,      1.0,  3.0,  4.0,  2.0     ]
    pre_robot_x_list = [ -4.0,   -4.0,   -4.0,  -4.0,  -4.0,  -4.0]
    pre_robot_y_list = [ -4.0,  -3.0, -1.0,  1.0,  3.0, 4.0]
    robot_type_array_list = [[0, 1, 0, 2, 3]]


robot_type_array = robot_type_array_list[i_case]

print('Task', flag_task, task_folder, ',\tCase', i_case, ',\tRepeat', i_repeat, ',\tRobot ', robot_type_array)

robot_type_num = len(robot_type_array)
block_type_num = len(block_type_array)

case_name = 'r'
for i_robot_type in range(robot_type_num):
    case_name += str(robot_type_array[i_robot_type])
case_name += '_b'
for i_block_type in range(block_type_num):
    case_name += str(block_type_array[i_block_type])

backup_launch_file = backup_folder + task_folder + backup_launch_folder + case_name + 'repeat' + str(i_repeat) + ".launch"
backup_case_file = backup_folder + task_folder + backup_case_folder + case_name + 'repeat' + str(i_repeat) + ".yaml"
backup_result_file = backup_folder + task_folder + backup_result_folder + case_name + 'repeat' + str(i_repeat) + ".yaml"
if not flag_backup:
    backup_result_file = './result.yaml'

# Genearte the robot param
robot_type_list = []
for i_robot_type in range(robot_type_num):
    for i_add in range(robot_type_array[i_robot_type]):
        robot_type_list.append(i_robot_type)
robot_num = len(robot_type_list)
robot_x_list = pre_robot_x_list[:robot_num]
robot_y_list = pre_robot_y_list[:robot_num]

# Genearte the block param
block_type_list = []
for i_block_type in range(block_type_num):
    for i_add in range(block_type_array[i_block_type]):
        block_type_list.append(i_block_type)
block_num = len(block_type_list)

if flag_task == 5:
    block_x_list = pre_block_x_list[i_repeat:i_repeat+1]
    block_y_list = pre_block_y_list[i_repeat:i_repeat+1]
else:
    block_x_list = pre_block_x_list[:block_num]
    block_y_list = pre_block_y_list[:block_num]

case_io = CaseIO()
# Change the test case parameters
case_io.robot_type_list = robot_type_list 
case_io.robot_x_list = robot_x_list
case_io.robot_y_list = robot_y_list
case_io.world_name = world_name

case_io.block_type_list = block_type_list 
case_io.block_x_list = block_x_list
case_io.block_y_list = block_y_list

case_io.result_yaml = backup_result_file

# Write the files
case_io.writeLaunch(launch_file)
case_io.writeYAML(yaml_file)
# case_io.readYAML(yaml_file)

if flag_backup:
    case_io.writeLaunch(backup_launch_file)
    case_io.writeYAML(backup_case_file)
