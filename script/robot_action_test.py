#!/usr/bin/env python
import sys
# sys.path.append("/home/bofu/Workspaces/sim_ws/src/teaming_simulation/src/teaming_simulation/")
import rospy
import numpy as np
from teaming_simulation.Robot import Robot
from teaming_simulation.msg import RobotAction
from teaming_simulation.Constant import Constant

def main():
    rospy.init_node('robot_action_test', anonymous=True)

    block_num = 3
    robot_index = 2
    robot_type = Constant.ROB_TYPE_LARGE_HEAVY
    priority = 1
    enable_visulization = True
    botOne = Robot(priority, block_num, robot_index, robot_type, enable_visulization)

    goal = np.array([2.0, 3.0])

    next_action_msg = RobotAction()
    next_action_msg.robot_id = robot_index
    next_action_msg.action_id = 1

    # Move
    # botOne.setGoal(goal)
    next_action_msg.action_type = Constant.ACTION_STATE_MOVE
    next_action_msg.goal_pose = goal

    # Pick
    next_action_msg.action_type = Constant.ACTION_STATE_PICK
    next_action_msg.block_ids = [0]

    # Explore
    # next_action_msg.action_type = Constant.ACTION_STATE_EXPLORE
    # next_action_msg.goal_region = [-0.71, -1.49, 1.55, 1.5] # xmin, ymin, xmax, ymax

    botOne.nextActionCallback(next_action_msg)
    print(next_action_msg)


    rospy.spin()

if __name__ == '__main__':
    print(sys.path)
    main()
