#!/usr/bin/env python3
import rospy
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from teaming_simulation.Arm import Arm

def main():
    rospy.init_node('arm_test', anonymous=True)
    robot_id = 2
    armone = Arm(robot_id)
    rate = rospy.Rate(5.0) # ROS Rate at 5Hz
    
    while not rospy.is_shutdown():
        # joint_angles = [[0.0, 0.6, 0.4, 0.5]]
        # gripper_states = [[1.0, 1.0]]

        # gripper_states = [[-1.0, -1.0], [-1.0, -1.0]]
        # joint_angles = [[0.0, 0.6, 0.4, 0.5], [0.0, -0.5, 0.5, 0.5]]

        gripper_states = [[1.0, 1.0], [-1.0, -1.0], [-1.0, -1.0]]
        joint_angles = [[0.0, 0.6, 0.4, 0.5], [0.0, 0.6, 0.4, 0.5], [0.0, -0.5, 0.5, 0.5]]

        # gripper_states = [[1.0, 1.0], [-1.0, -1.0]]
        # joint_angles = [[0.0, 0.6, 0.4, 0.5], [0.0, 0.6, 0.4, 0.5]]

        # deg2rad = 3.141592653589 / 180.0
        # gripper_states = [[-1.0, -1.0]]
        # joint_angles = [[-70*deg2rad, -56*deg2rad, 51*deg2rad, 68*deg2rad]]

        # gripper_states = [[1.0, 1.0]]
        # joint_angles = [[-70*deg2rad, -56*deg2rad, 51*deg2rad, 68*deg2rad]]

        # joint_angles = [[0.0, 0.0, 0.0, 0.0]]
        # gripper_states = [0.0]

        # joint_angles = [[0.0, 0.6, 0.4, 0.5]]
        # gripper_states = [-0.3]

        # joint_angles = [[0.0, -0.5, 0.5, 0.5]]
        # gripper_states = []

        # joint_angles = [[0.0, 0.5, 0.5, 0.5],[0.0, -0.5, 0.5, 0.5]]
        # joint_angles = [[0.0, 0.5, 0.5, 0.5],[0.0, -0.5, 0.5, 0.5],[1, -0.5, 0.5, 0.5]]
        # gripper_states = [0.8, -0.5, 0.8]
        total_time = 5.0
        armone.publish_commands(joint_angles, gripper_states, total_time)
        # armone.publish_commands([[0.0, 0.5, 0.5, 0.5, 0.0],[0.0, -0.5, 0.5, 0.5, 1.0],[1, -0.5, 0.5, 0.5, 0.0]],5)
        rospy.sleep(total_time)


if __name__ == '__main__':
    main()
